﻿namespace Catalyte.Apparel.DTOs
{
    /// <summary>
    /// Describes a data transfer object for a promo code.
    /// </summary>
    public class PromoCodesDTO
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public string Rate { get; set; }
    }
}
