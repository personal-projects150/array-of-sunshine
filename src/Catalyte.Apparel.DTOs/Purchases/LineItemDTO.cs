﻿namespace Catalyte.Apparel.DTOs.Purchases
{
    /// <summary>
    /// Describes a data transfer object for a single line item of a purchase transaction.
    /// </summary>
    public class LineItemDTO
    {
        public int ProductId { get; set; }

        public int Quantity { get; set; }
        
        public string ProductName { get; set; }

    }
}
