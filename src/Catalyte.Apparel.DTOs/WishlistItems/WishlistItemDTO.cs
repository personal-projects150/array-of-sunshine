﻿using System;

namespace Catalyte.Apparel.DTOs.WishlistItems
{
    /// <summary>
    /// Describes a data transfer object for a wishlist item linked to user ID.
    /// </summary>

    public class WishlistItemDTO
    {
        public int Id { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }

        public int UserId { get; set; }
    }
}
