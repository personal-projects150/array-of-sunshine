﻿using System;

namespace Catalyte.Apparel.DTOs.ProductReviews
{
    /// <summary>
    /// Describes a data transfer object for a product review.
    /// </summary>
    public class ProductReviewDTO
    {
        public int Id { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        public string Title { get; set; }

        public int Rating { get; set; }

        public DateTime DatePosted { get; set; }

        public string Comment { get; set; }

        public int ProductId { get; set; }

        public int UserId { get; set; }
    }
}
