﻿using System;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.SeedData;
using Catalyte.Apparel.Providers.Providers;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Unit
{
    public class ProductFactoryTest
    {
        private readonly ProductFactory _factory = new();
        private readonly Data.Model.Product testProduct;
        private readonly List<Data.Model.Product> testProducts;

        public ProductFactoryTest()
        {
            testProduct = _factory.CreateRandomProduct(1);
            testProducts = _factory.GenerateRandomProducts(4);



        }

        [Fact]
        public void CheckDemographic_ContainsDemographicFromList()
        {
            Assert.Contains(testProduct.Demographic, _factory._demographics);
        }

        [Fact]
        public void CheckCategory_ContainsCategoryFromList()
        {
            Assert.Contains(testProduct.Category, _factory._categories);
        }

        [Fact]
        public void CheckType_ContainsTypeFromList()
        {
            Assert.Contains(testProduct.Type, _factory._types);
        }

        [Fact]
        public void CheckPrimaryColor_ContainsColorFromList()
        {
            Assert.Contains(testProduct.PrimaryColorCode, _factory._colors);
        }

        [Fact]
        public void CheckSecondaryColor_ContainsColorFromList()
        {
            Assert.Contains(testProduct.SecondaryColorCode, _factory._colors);
        }

        [Fact]
        public void CheckGlobalProductCode_StartsWithPO()
        {
            Assert.StartsWith("po-", testProduct.GlobalProductCode);
        }

        [Fact]
        public void CheckGlobalProductCode_ExactlyTenCharacters()
        {
            Assert.Equal(10, testProduct.GlobalProductCode.Length);
        }

        [Fact]
        public void CheckStyleCode_StartsWithSC()
        {
            Assert.StartsWith("sc", testProduct.StyleNumber);
        }

        [Fact]
        public void CheckStyleCode_ExactlySevenCharacters()
        {
            Assert.Equal(7, testProduct.StyleNumber.Length);
        }

        [Fact]
        public void CheckBrand_ContainsBrandFromList()
        {
            Assert.Contains(testProduct.Brand, _factory._brands);
        }

        [Fact]
        public void CheckMaterial_ContainsMaterialFromList()
        {
            Assert.Contains(testProduct.Material, _factory._materials);
        }

        [Fact]
        public void CheckPrice_IsOnlyBetween_OneAnd500()
        {
            Assert.InRange(testProduct.Price, 1, 500);
        }

        [Fact]
        public void CheckQuantity_IsOnlyBetween_ZeroAnd1000()
        {
            Assert.InRange(testProduct.Quantity, 0, 1000);
        }

        [Fact]
        public void ReleaseDate_IsNotNull()
        {
            Assert.NotNull(testProduct.ReleaseDate);
        }

        [Fact]
        public void ActiveStatus_IsNotNull()
        {
            Assert.NotNull(testProduct.Active);
        }

        [Fact]
        public void CheckDescription_ContainsCategoryAndDemographicFromList()
        {
            Assert.Contains(testProduct.Category, testProduct.Description);
            Assert.Contains(testProduct.Demographic, testProduct.Description);

        }

        [Fact]
        public void CheckName_ContainsCategoryAndTypeFromList()
        {
            Assert.Contains(testProduct.Category, testProduct.Name);
            Assert.Contains(testProduct.Type, testProduct.Name);

        }

    }
}

