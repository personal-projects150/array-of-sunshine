﻿using AutoMapper;
using Catalyte.Apparel.API;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.SeedData;
using Catalyte.Apparel.DTOs.Products;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Catalyte.Apparel.Test.Unit.DataMappingTests
{
    /// <summary>
    /// Provides tests for the mapping from Product to ProductDTO
    /// to ensure that the AverageRating property of ProductDTO
    /// is calculated correctly.
    /// </summary>
    public class ProductReviewAverageRatingTest
    {
        private readonly IMapper _mapper;
        private readonly ProductFactory _productFactory = new();
        private readonly Product _testProduct;
        private readonly ProductReviewFactory _productReviewFactory;
        private readonly List<ProductReview> _testReviews;

        private readonly List<User> _users = new()
        {
            new User()
            {
                Id = 1,
                Email = "ajokerst@catalyte.io",
                FirstName = "Abbie",
                LastName = "Jokerst",
                Street = "123 Abbey Rd",
                City = "Sunshine",
                State = "Colorado",
                Zip = "12345"
            },
            new User()
            {
                Id = 2,
                Email = "kmacgregor@catalyte.io",
                FirstName = "Kyle",
                LastName = "MacGregor",
                Street = "21 Bag End Lane",
                City = "Hobbiton",
                State = "Shire",
                Zip = "12345"
            }
        };
        public ProductReviewAverageRatingTest()
        {
            _testProduct = _productFactory.CreateRandomProduct(1);
            _productReviewFactory = new ProductReviewFactory(new List<Product>() { _testProduct }, _users);
            _testReviews = _productReviewFactory.GenerateRandomReviews();
            _testProduct.Reviews = _testReviews;

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MapperProfile());
            });

            _mapper = config.CreateMapper();
        }

        [Fact]
        public void ProductDTO_Exists()
        {
            var dto = _mapper.Map<ProductDTO>(_testProduct);

            Assert.NotNull(dto);
            Assert.IsType<ProductDTO>(dto);
        }

        [Fact]
        public void ProductDTO_AverageRating_IsCorrectType()
        {
            var dto = _mapper.Map<ProductDTO>(_testProduct);

            Assert.IsType<decimal>(dto.AverageRating);
        }

        [Fact]
        public void ProductDTO_AverageRating_HasCorrectValue()
        {
            // Arrange
            var dto = _mapper.Map<ProductDTO>(_testProduct);
            var count = _testReviews.Count();
            var sum = _testReviews.Select(review => review.Rating).Sum();
            var expected = (decimal)sum / count;

            // Act
            var actual = dto.AverageRating;

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
