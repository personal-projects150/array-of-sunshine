﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.SeedData;
using Catalyte.Apparel.Providers.Providers;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Unit.Providers.PromoCode
{
    public class PromoCodeProviderTests
    {
        private readonly ProductFactory _factory = new();

        private readonly Mock<IPromoCodeRepository> repositoryStub;
        private readonly Mock<ILogger<PromoCodeProvider>> loggerStub;

        private readonly PromoCodeProvider provider;

        private readonly Data.Model.PromoCode testPromoCodeWrongType;
        private readonly Data.Model.PromoCode testPromoCodeWrongNumber;
        private readonly Data.Model.PromoCode testPromoCodeWrongFormat;
        private readonly Data.Model.PromoCode testPromoCodeProperInput;

        public PromoCodeProviderTests()
        {
            repositoryStub = new Mock<IPromoCodeRepository>();
            loggerStub = new Mock<ILogger<PromoCodeProvider>>();
            provider = new PromoCodeProvider(repositoryStub.Object, loggerStub.Object);

            testPromoCodeWrongType = new Data.Model.PromoCode
            {
                Title = "SUMMER2015",
                Description = "Our summer discount for the Q3 2015 campaign",
                Type = "wrong type",
                Rate = "100"
            };

            testPromoCodeWrongNumber = new Data.Model.PromoCode
            {
                Title = "SUMMER2015",
                Description = "Our summer discount for the Q3 2015 campaign",
                Type = "percent",
                Rate = "500"
            };

            testPromoCodeWrongFormat = new Data.Model.PromoCode
            {
                Title = "SUMMER2015",
                Description = "Our summer discount for the Q3 2015 campaign",
                Type = "flat",
                Rate = "500"
            };

            testPromoCodeProperInput = new Data.Model.PromoCode
            {
                Title = "SUMMER2015",
                Description = "Our summer discount for the Q3 2015 campaign",
                Type = "flat",
                Rate = "500.00"
            };
        }

        [Fact]
        public async Task CreatePromoCodeAsync_WithWrongType_ReturnsWrongTypeException()
        {
            //Act

            Func<Task> result = async () => { await provider.CreatePromoCodeAsync(testPromoCodeWrongType); };

            //Assert

            await result.Should().ThrowAsync<ConflictException>();

        }

        [Fact]
        public async Task CreatePromoCodeAsync_WithWrongNumber_ReturnsWrongNumberException()
        {
            //Act

            Func<Task> result = async () => { await provider.CreatePromoCodeAsync(testPromoCodeWrongNumber); };

            //Assert

            await result.Should().ThrowAsync<ConflictException>();

        }

        [Fact]
        public async Task CreatePromoCodeAsync_WithWrongFormat_ReturnsWrongFormatException()
        {
            //Act

            Func<Task> result = async () => { await provider.CreatePromoCodeAsync(testPromoCodeWrongFormat); };

            //Assert

            await result.Should().ThrowAsync<ConflictException>();

        }

    }
}
