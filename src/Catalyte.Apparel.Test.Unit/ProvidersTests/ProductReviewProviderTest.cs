﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.SeedData;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Providers.Providers;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Unit.Providers.Product
{
    public class ProductReviewProviderTest
    {
        private readonly ProductFactory _productFactory = new();
        private readonly ProductReviewFactory _reviewFactory;
        private readonly List<User> _users = new()
        {
            new User()
            {
                Id = 1,
                Email = "ajokerst@catalyte.io",
                FirstName = "Abbie",
                LastName = "Jokerst",
                Street = "123 Abbey Rd",
                City = "Sunshine",
                State = "Colorado",
                Zip = "12345"
            },
            new User()
            {
                Id = 2,
                Email = "kmacgregor@catalyte.io",
                FirstName = "Kyle",
                LastName = "MacGregor",
                Street = "21 Bag End Lane",
                City = "Hobbiton",
                State = "Shire",
                Zip = "12345"
            }
        };

        private readonly Mock<IProductReviewRepository> repositoryStub;
        private readonly Mock<IProductRepository> productRepositoryStub;
        private readonly Mock<IUserRepository> userRepositoryStub;
        private readonly Mock<IProductProvider> productProviderStub;
        private readonly Mock<ILogger<ProductReviewProvider>> loggerStub;

        private readonly ProductReviewProvider provider;

        private readonly Data.Model.Product testProduct;
        private readonly List<Data.Model.Product> testProducts;
        private readonly ProductReview testReview;
        private readonly List<ProductReview> testReviews;

        public ProductReviewProviderTest()
        {
            // Set up initial testing tools that most/all tests will use
            repositoryStub = new Mock<IProductReviewRepository>();
            productProviderStub = new Mock<IProductProvider>();
            loggerStub = new Mock<ILogger<ProductReviewProvider>>();
            productRepositoryStub = new Mock<IProductRepository>();
            userRepositoryStub = new Mock<IUserRepository>();
            provider = new ProductReviewProvider(repositoryStub.Object, loggerStub.Object, productRepositoryStub.Object, productProviderStub.Object, userRepositoryStub.Object);

            testProduct = _productFactory.CreateRandomProduct(1);
            var testProductList = new List<Data.Model.Product>() { testProduct };
            _reviewFactory = new ProductReviewFactory(testProductList, _users);
            testReview = _reviewFactory.CreateRandomReview(1, testProduct);

            repositoryStub.Setup(repo => repo.GetProductReviewByIdAsync(1))
                .ReturnsAsync(testReview);


            testProducts = _productFactory.GenerateRandomProducts(4);
            _reviewFactory = new ProductReviewFactory(testProducts, _users);
            testReviews = _reviewFactory.GenerateRandomReviews();

            repositoryStub.Setup(repo => repo.GetAllProductReviewsAsync())
                .ReturnsAsync(testReviews);
            repositoryStub.Setup(repo => repo.GetProductReviewsByProductIdAsync(1))
                .ReturnsAsync(testReviews.Where(review => review.ProductId == 1));
            repositoryStub.Setup(repo => repo.GetProductReviewsByProductIdAsync(999))
                .ReturnsAsync(testReviews.Where(review => review.ProductId == 999));

        }

        /// <summary>
        /// Tests that reviews can be retrieved by id.
        /// </summary>
        [Fact]
        public async Task GetReviewByIdAsync_WithExistingReview_ReturnsExpectedReview()
        {
            // Act
            var result = await provider.GetProductReviewByIdAsync(1);

            // Assert
            result.Should().BeEquivalentTo(
                testReview,
                options => options.ComparingByMembers<ProductReview>());
        }

        /// <summary>
        /// Tests that an invalid id returns a NotFoundException.
        /// </summary>
        [Fact]
        public async Task GetProductReviewByIdAsync_WithNonExistingReview_ReturnsNotFoundException()
        {
            // Act
            Func<Task> nonExistingReview = async () => { await provider.GetProductReviewByIdAsync(99999999); };
            // Assert
            await nonExistingReview.Should().ThrowAsync<NotFoundException>();
        }

        /// <summary>
        /// Tests that all reviews can be retrieved.
        /// </summary>
        [Fact]
        public async Task GetAllProductReviewsAsync_ReturnsAllReviews()
        {
            // Act
            var results = await provider.GetAllProductReviewsAsync();
            // Assert
            results.Should().BeEquivalentTo(testReviews);
        }

        /// <summary>
        /// Tests that reviews can be retrieved by product id.
        /// </summary>
        [Fact]
        public async Task GetProductReviewsByProductIdAsync_WithProductId_ReturnsExpectedReviews()
        {
            var results = await provider.GetProductReviewsByProductIdAsync(1);
            results.Should().BeEquivalentTo(testReviews.Where(review => review.ProductId == 1));
        }

        /// <summary>
        /// Tests that, given a valid product id, where the product has no reviews,
        /// an empty collection is returned.
        /// </summary>
        [Fact]
        public async Task GetProductReviewsByProductIdAsync_WithNoReviews_ReturnsEmpty()
        {
            var product = _productFactory.CreateRandomProduct(999);
            var results = await provider.GetProductReviewsByProductIdAsync(product.Id);

            results.Should().BeEmpty();
        }

       
        /// <summary>
        /// Tests that an invalid review id returns a NotFoundException.
        /// </summary>
        [Fact]
        public async Task DeleteProductReviewByIdAsync_WithInvalidId_ReturnsNotFoundException()
        {
            // Act
            Func<Task> nonExistingReview = async () => { await provider.DeleteProductReviewByIdAsync(99999999); };
            // Assert
            await nonExistingReview.Should().ThrowAsync<NotFoundException>();
        }
    }
}