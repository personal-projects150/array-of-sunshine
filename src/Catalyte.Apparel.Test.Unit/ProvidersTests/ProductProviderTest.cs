using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.SeedData;
using Catalyte.Apparel.Providers.Providers;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Unit.Providers.Product
{
    public class ProductProviderTests
    {
        private readonly ProductFactory _factory = new();

        private readonly Mock<IProductRepository> repositoryStub;
        private readonly Mock<ILogger<ProductProvider>> loggerStub;

        private readonly ProductProvider provider;

        // Default ProductFilterParams returns all products
        private readonly ProductFilterParams defaultProductFilterParams = new();
        private readonly Data.Model.Product testProduct;
        private readonly Data.Model.Product nullProduct;
        private readonly List<Data.Model.Product> testProducts;

        public ProductProviderTests()
        {
            // Set up initial testing tools that most/all tests will use
            repositoryStub = new Mock<IProductRepository>();
            loggerStub = new Mock<ILogger<ProductProvider>>();
            provider = new ProductProvider(repositoryStub.Object, loggerStub.Object) ;

            testProduct = _factory.CreateRandomProduct(1);
            repositoryStub.Setup(repo => repo.GetProductByIdAsync(1))
                .ReturnsAsync(testProduct);

            testProducts = _factory.GenerateRandomProducts(4);
            repositoryStub.Setup(repo => repo.GetProductsAsync(defaultProductFilterParams))
                .ReturnsAsync(testProducts);
            nullProduct = new Data.Model.Product {  };
        }

        [Fact]
        public async Task GetProductsByIdAsync_WithExistingProduct_ReturnsExpectedProduct()
        {
            // Arrange


            // Act
            var result = await provider.GetProductByIdAsync(1);

            // Assert
            result.Should().BeEquivalentTo(
                testProduct,
                options => options.ComparingByMembers<Data.Model.Product>());
        }

        [Fact]
        public async Task GetProductsByIdAsync_WithNonExistingProduct_ReturnsNotFoundException()
        {
            // Arrange


            // Act

            Func<Task> nonExistingProduct = async () => { await provider.GetProductByIdAsync(99999999); };

            // Assert 

            await nonExistingProduct.Should().ThrowAsync<NotFoundException>();
        }

        /// <summary>
        /// Tests that a valid created product returns a saved product
        /// </summary>
        [Fact]
        public async Task CreateProductAsync_WithValidProduct_ReturnsExpectedProduct()
        {
            //Arrange
            
            //Act
            var result = await provider.CreateProductAsync(testProduct);
            //Assert
            result.Should().BeEquivalentTo(testProduct, options => options.ComparingByMembers<Data.Model.Product>());
        }

        /// <summary>
        /// Tests that an exception is thrown when given an empty product
        /// </summary>
        [Fact]
        public async Task CreateProductAsync_WithNullProduct_ReturnsBadRequestException()
        {
            //Arrange
            //Act
            Func<Task> result = async () => { await provider.CreateProductAsync(nullProduct); };
            //Assert
            await result.Should().ThrowAsync<BadRequestException>();
        }
    }
}