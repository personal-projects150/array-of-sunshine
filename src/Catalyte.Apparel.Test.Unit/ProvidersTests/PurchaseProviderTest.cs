﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Providers.Providers;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;


namespace Catalyte.Apparel.Test.Unit.Provider.Purchase
{
    public class PurchaseProviderTests
    {
        private readonly Mock<IPurchaseRepository> repositoryStub;
        private readonly Mock<ILogger<PurchaseProvider>> loggerStub;
        private readonly Mock<IProductRepository> productRepositoryStub;
        private readonly IPurchaseProvider purchaseProvider;
       
        private readonly Data.Model.Purchase testPurchase;
        private readonly Data.Model.Purchase testPurchaseSameEmail;
        private readonly Data.Model.Purchase testPurchaseDiffEmail;
        private readonly List<Data.Model.Purchase> testPurchases;

        private readonly Data.Model.Product testProductInactive1;
        private readonly Data.Model.Product testProductInactive2;
        private readonly Data.Model.Product testProductActive;
        private readonly List<Data.Model.Product> testProducts;
        private readonly List<Data.Model.LineItem> testLineItemInactiveSingle;
        private readonly List<Data.Model.LineItem> testLineItemInactiveMultiple;
        private readonly List<Data.Model.LineItem> testLineItemInactiveAndActive;
        private readonly List<Data.Model.LineItem> testLineItemActive;
        private readonly List<Data.Model.LineItem> testLineItemNull;


        private readonly Data.Model.Purchase testPurchaseSingleInactive;
        private readonly Data.Model.Purchase testPurchaseMultipleInactive;
        private readonly Data.Model.Purchase testPurchaseInactiveAndActive;
        private readonly Data.Model.Purchase testPurchaseActive;
        private readonly Data.Model.Purchase testPurchaseNull;


        private readonly PurchaseProvider provider;
        private readonly Data.Model.Purchase validateValidPurchase;
        

        public PurchaseProviderTests()
        {
            // Arrange
            // Create some purchases 
            repositoryStub = new Mock<IPurchaseRepository>();
            productRepositoryStub = new Mock<IProductRepository>();
            loggerStub = new Mock<ILogger<PurchaseProvider>>();

            purchaseProvider = new PurchaseProvider(repositoryStub.Object, loggerStub.Object, productRepositoryStub.Object);
            
            provider = new PurchaseProvider(repositoryStub.Object, loggerStub.Object, productRepositoryStub.Object);

            testPurchase = new Data.Model.Purchase { BillingEmail = "abc@123.org", CardHolder = "Indiana Jones" };
            testPurchaseSameEmail = new Data.Model.Purchase { BillingEmail = "abc@123.org", CardHolder = "Kentucky Smith" };
            testPurchaseDiffEmail = new Data.Model.Purchase { BillingEmail = "def@123.org", CardHolder = "New Mexico Andy" };
            testPurchases = new List<Data.Model.Purchase> { testPurchase, testPurchaseSameEmail, testPurchaseDiffEmail };

            testProductInactive1 = new Data.Model.Product { Id = 1, Active = false, Name = "Boot Shoes", };
            testProductInactive2 = new Data.Model.Product { Id = 2, Active = false, Name = "Sassy Pants" };
            testProductActive = new Data.Model.Product { Id = 3, Active = true, Name = "Doggy Shirt" };
            testProducts = new List<Data.Model.Product> { testProductInactive1, testProductInactive2, testProductActive };

            testLineItemInactiveSingle = new List<Data.Model.LineItem> { new Data.Model.LineItem { ProductId = testProductInactive1.Id } };
            testLineItemInactiveMultiple = new List<Data.Model.LineItem> { new Data.Model.LineItem { ProductId = testProductInactive2.Id }, new Data.Model.LineItem { ProductId = testProductInactive1.Id } };
            testLineItemInactiveAndActive = new List<Data.Model.LineItem> { new Data.Model.LineItem { ProductId = testProductActive.Id }, new Data.Model.LineItem { ProductId = testProductInactive1.Id }, new Data.Model.LineItem { ProductId = testProductInactive2.Id } };
            testLineItemActive = new List<Data.Model.LineItem> { new Data.Model.LineItem { ProductId = testProductActive.Id } };
            testLineItemNull = new List<Data.Model.LineItem> { new Data.Model.LineItem { ProductId = 6000 } };


            testPurchaseSingleInactive = new Data.Model.Purchase { LineItems = testLineItemInactiveSingle };
            testPurchaseMultipleInactive = new Data.Model.Purchase { LineItems = testLineItemInactiveMultiple };
            testPurchaseInactiveAndActive = new Data.Model.Purchase { LineItems = testLineItemInactiveAndActive };
            testPurchaseActive = new Data.Model.Purchase { LineItems = testLineItemActive };
            testPurchaseNull = new Data.Model.Purchase { LineItems = testLineItemNull };

            validateValidPurchase = new Data.Model.Purchase { DeliveryFirstName = "Firstname", DeliveryLastName = "Lastname", DeliveryStreet = "Ocean Ave", DeliveryCity = "Waimea", DeliveryZip = "12345", BillingStreet = "Sea Turle Way", BillingCity = "Wailua", BillingZip = "12345-6789", BillingPhone = "123 456-7890", BillingEmail = "user@domain.com", CardHolder = "First Last", CardNumber = "4485500989350421", CVV = "789", Expiration = "12/25", LineItems = testLineItemActive };
           

            repositoryStub.Setup(x => x.GetAllPurchasesAsync()).ReturnsAsync(testPurchases);
            productRepositoryStub.Setup(x => x.GetProductByIdAsync(3)).ReturnsAsync(testProductActive);
            productRepositoryStub.Setup(x => x.GetProductByIdAsync(2)).ReturnsAsync(testProductInactive2);
            productRepositoryStub.Setup(x => x.GetProductByIdAsync(1)).ReturnsAsync(testProductInactive1);
            repositoryStub.Setup(x => x.CreatePurchaseAsync(validateValidPurchase)).ReturnsAsync(validateValidPurchase);
        }

        [Fact]
        public async Task GetAllPurchasesAsync_WithExistingPurchases_ReturnsPurchaseList()
        {
            // Act
            // Retrieve the set of purchases by email
            var result = await purchaseProvider.GetAllPurchasesAsync("abc@123.org");
            // Assert
            // The result should be the purchases attached to abc@123.org and not the one attached to def@123.org
            foreach (var purchase in testPurchases)
            {
                if (purchase.BillingEmail == "abc@123.org")
                {
                    result.Should().Contain(purchase);
                }
                else
                {
                    result.Should().NotContain(purchase);
                }
            }
        }

        [Fact]
        public async Task GetAllPurchasesAsync_WithoutPurchases_ReturnsEmptyPurchaseList()
        {
            // Act
            // Retrieve the set of purchases by email
            var result = await purchaseProvider.GetAllPurchasesAsync("ghi@123.org");
            // Assert
            // The result should be an empty enumerable
            result.Count().Should().Be(0);
        }
        
        [Fact]
        public async Task InactiveProductException_WithInactiveProduct_ThrowsError()
        {
            Func<Task> result = async () => { await purchaseProvider.InactiveProductException(testPurchaseSingleInactive); };
            await result.Should().ThrowAsync<UnprocessableEntityException>();
        }

        [Fact]
        public async Task InactiveProductException_WithInactiveProducts_ThrowsError()
        {
            Func<Task> result = async () => { await purchaseProvider.InactiveProductException(testPurchaseMultipleInactive); };
            await result.Should().ThrowAsync<UnprocessableEntityException>();
        }

        [Fact]
        public async Task InactiveProductException_WithActiveAndInactiveProducts_ThrowsError()
        {
            Func<Task> result = async () => { await purchaseProvider.InactiveProductException(testPurchaseInactiveAndActive); };
            await result.Should().ThrowAsync<UnprocessableEntityException>();
        }

        [Fact]
        public async Task InactiveProductException_WithActiveProduct_ReturnsPurchase()
        {
            var result = await purchaseProvider.InactiveProductException(testPurchaseActive);
            result.Should().Be(testPurchaseActive);
        }

        [Fact]
        public async Task InactiveProductException_WithNonexistentProduct_ThrowsError()
        {
            Func<Task> result = async () => { await purchaseProvider.InactiveProductException(testPurchaseNull); };
            await result.Should().ThrowAsync<UnprocessableEntityException>();
        }

        [Fact]
        public void ValidiateIfEmptyOrNull_ReturnsTrueIfEmpty()
        {
            var given = string.Empty;
            var actual = provider.ValidateIfEmptyOrNull(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfEmptyOrNull_ReturnsTrueIfNull()
        {
            string given = null;
            var actual = provider.ValidateIfEmptyOrNull(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfEmptyOrNull_ReturnsTrueIfOnlySpaces()
        {
            var given = "   ";
            var actual = provider.ValidateIfEmptyOrNull(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfEmptyOrNull_ReturnsFalseIfNotEmptyOrNull()
        {
            var given = "This is my test string with text, 123456789, )*&%@)#%";
            var actual = provider.ValidateIfEmptyOrNull(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateIfEmptyOrNull_ReturnsTrueIfContainsWhitespace()
        {
            var given = "   ";
            var actual = provider.ValidateIfEmptyOrNull(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenAndApostropheOnly_ReturnsTrueWithApostrophe()
        {
            var given = "Timothy O'Niel";
            var actual = provider.ValidateIfAlphabeticHyphenAndApostropheOnly(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenAndApostropheOnly_ReturnsTrueWithHyphen()
        {
            var given = "Beth-Anne Wallace";
            var actual = provider.ValidateIfAlphabeticHyphenAndApostropheOnly(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenAndApostropheOnly_ReturnsFalseIfNumeric()
        {
            var given = "1243450";
            var actual = provider.ValidateIfAlphabeticHyphenAndApostropheOnly(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenAndApostropheOnly_ReturnsTrueIfContainsAllValidCharacters()
        {
            var given = "Terry-Anne O'Brien";
            var actual = provider.ValidateIfAlphabeticHyphenAndApostropheOnly(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateCreditCardSixteenNumericalDigits_ReturnsTrueIfSixteenDigitsExactly()
        {
            var given = "1234567812345678";
            var actual = provider.ValidateCreditCardSixteenNumericalDigits(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateCreditCardSixteenNumericalDigits_ReturnsFalseIfLessThanSixteenDigits()
        {
            var given = "12345678";
            var actual = provider.ValidateCreditCardSixteenNumericalDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCreditCardSixteenNumericalDigits_ReturnsFalseIfMoreThanSixteenDigits()
        {
            var given = "1234456789123456789";
            var actual = provider.ValidateCreditCardSixteenNumericalDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCreditCardSixteenNumericalDigits_ReturnsFalseIfAlphabeticCharactersIncluded()
        {
            var given = "abc123efg456hijk";
            var actual = provider.ValidateCreditCardSixteenNumericalDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCreditCardSixteenNumericalDigits_ReturnsFalseIfIncludesSymbols()
        {
            var given = "123!@#456$%^789&";
            var actual = provider.ValidateCreditCardSixteenNumericalDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCreditCardIsLuhnValid_ReturnsTrueIfValidCreditCardNumber()
        {
            var given = "4024007135864709";
            var actual = provider.ValidateCreditCardIsLuhnValid(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateCreditCardIsLuhnValid_ReturnsFalseifInvalidCreditCardNumber()
        {
            var given = "1435678998761234";
            var actual = provider.ValidateCreditCardIsLuhnValid(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCVVIsThreeNumericOnlyDigits_ReturnsTrueIfOnlyThreeNumericDigits()
        {
            var given = "546";
            var actual = provider.ValidateCVVIsThreeNumericOnlyDigits(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateCVVIsThreeNumericOnlyDigits_ReturnsFalseIfMoreThanThreeNumericDigits()
        {
            var given = "0909";
            var actual = provider.ValidateCVVIsThreeNumericOnlyDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCVVIsThreeNumericOnlyDigits_ReturnsFalseIfLessThanThreeNumericDigits()
        {
            var given = "12";
            var actual = provider.ValidateCVVIsThreeNumericOnlyDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCVVIsThreeNumericOnlyDigits_ReturnsFalseIfAlphabeticCharactersIncluded()
        {
            var given = "4B7";
            var actual = provider.ValidateCVVIsThreeNumericOnlyDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateCVVIsThreeNumericOnlyDigits_ReturnsFalseIfSymbolsIncluded()
        {
            var given = "1@3";
            var actual = provider.ValidateCVVIsThreeNumericOnlyDigits(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateExpirationDateFormat_ReturnsTrueIfValidMMYYFormat()
        {
            var given = "03/25";
            var actual = provider.ValidateExpirationDateFormat(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateExpirationDateFormat_ReturnsFalseIfYYInWrongFormat()
        {
            var given = "03/2025";
            var actual = provider.ValidateExpirationDateFormat(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateExpirationDateFormat_ReturnsFalseIfMMOnlyOneDigit()
        {
            var given = "3/25";
            var actual = provider.ValidateExpirationDateFormat(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateExpirationDateFormat_ReturnsFalseIfInvalidMonth()
        {
            var given = "13/25";
            var actual = provider.ValidateExpirationDateFormat(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateExpirationDateFormat_ReturnsFalseIfDateNotSeparatedWithForwardSlash()
        {
            var given = "0325";
            var actual = provider.ValidateExpirationDateFormat(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateExpirationDateFormat_ReturnsFaseIfDateSeparatedByIncorrectSyntax()
        {
            var given = "03-25";
            var actual = provider.ValidateExpirationDateFormat(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateExpirationDateIsInTheFuture_ReturnsTrueIfDateIsInTheFuture()
        {
            var given = "04/25";
            var actual = provider.ValidateExpirationDateIsInTheFuture(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateExpirationDateIsInTheFuture_ReturnsFalseIfDateIsInThePast()
        {
            var given = "05/07";
            var actual = provider.ValidateExpirationDateIsInTheFuture(given);
            actual.Should().BeFalse();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly_ReturnsTrueIfValid()
        {
            var given = "123 Test -.'";
            var actual = provider.ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly_ReturnsFalseIfInvalid()
        {
            var given = "+_043957a;erkjg @&#%$ '.-";
            var actual = provider.ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenApostropheAndPeriodOnly_ReturnsTrueifValid()
        {
            var given = "This is a CITY -'.";
            var actual = provider.ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(given);
            actual.Should().BeTrue();
        }

        [Fact]
        public void ValidateIfAlphabeticHyphenApostropheAndPeriodOnly_ReturnsFalseIfInvalid()
        {
            var given = "This is also a CITY -'.*&^$,";
            var actual = provider.ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain_ReturnsTrueIfUsernameAndDomainValid()
        {
            var given = "user@domainofchoice.com";
            var actual = provider.ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(given);
            Assert.True(actual);
        }

        [Fact]
        public void ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain_ReturnsTrueWithValidSubDomain()
        {
            var given = "user@domainofchoice.subdomain.edu";
            var actual = provider.ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(given);
            Assert.True(actual);
        }

        [Fact]
        public void ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain_ReturnsFalseIfUsernameInvalid()
        {
            var given = "user)*&123@domainofchoice.net";
            var actual = provider.ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain_ReturnsFalseIfDomainInvalid()
        {
            var given = "user@123domainofchoice.com";
            var actual = provider.ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain_ReturnsFalseIfSubDomainInvalid()
        {
            var given = "username@domain.123subdomain.com";
            var actual = provider.ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain_ReturnsFalseIfInvalidFormat()
        {
            var given = "@userdomain.com";
            var actual = provider.ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidateZipCodeIsOnlyFiveOrNineDigits_ReturnsTrueIfValid()
        {
            var given = "12345";
            var actual = provider.ValidateZipCodeIsOnlyFiveOrNineDigits(given);
            Assert.True(actual);
        }

        [Fact]
        public void ValidateZipCodeIsOnlyFiveOrNineDigits_ReturnsFalseIfInvalid()
        {
            var given = "1234-";
            var actual = provider.ValidateZipCodeIsOnlyFiveOrNineDigits(given);
            Assert.False(actual);
        }

        [Fact]
        public void ValidatePhoneNumberIsTenDigits_ReturnsTrueIfValid()
        {
            var given = "(000) 000-0000";
            var actual = provider.ValidatePhoneNumberIsTenDigits(given);
            Assert.True(actual);
        }

        [Fact]
        public void ValidatePhoneNumberIsTenDigits_ReturnsTrueWithoutSyntaxFormatting()
        {
            var given = "0000000000";
            var actual = provider.ValidatePhoneNumberIsTenDigits(given);
            Assert.True(actual);
        }

        [Fact]
        public void ValidatePhoneNumberIsTenDigits_ReturnsFalseIfInvalid()
        {
            var given = "(00 00 - 0000";
            var actual = provider.ValidatePhoneNumberIsTenDigits(given);
            Assert.False(actual);
        }

        [Fact]
        public void RemoveSpecialCharactersFromNumericalString_ReturnsStringWithoutCharactersOnlyNumbers()
        {
            var given = "(000) 000-0000";
            var actual = provider.RemoveSpecialCharactersFromNumericalString(given);
            var expected = "0000000000";
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ValidatePurchaseInputFields_ReturnsTrueIfListContainsWhatItIsSupposedTo()
        {   //The int is a placeholder only in order to Func to cooperate, return 0; is never (in theory) reached
            Func<int> validPurchase = () => { provider.ValidatePurchaseInputFields(validateValidPurchase); return 0; };
            validPurchase.Should().NotThrow<BadRequestException>();
        }

        [Fact]
        public void ValidatePurchaseInputFields_ThrowsBadRequestExceptionIfInvalidFields()
        {
            validateValidPurchase.BillingEmail = "user)*&^@09domain.com";
            validateValidPurchase.BillingPhone = "(123) 45678";
            validateValidPurchase.BillingStreet = "()*&^ Test Ave.";
            validateValidPurchase.BillingCity = "*&^% City Name";
            validateValidPurchase.BillingZip = "987-";
            validateValidPurchase.DeliveryStreet = "Random Invalid Street )(*^";
            validateValidPurchase.DeliveryCity = "123 City";
            validateValidPurchase.DeliveryZip = "12345-09877";
            validateValidPurchase.CardHolder = "^&%$ Name Here";
            validateValidPurchase.CardNumber = "1234123412341234";
            validateValidPurchase.CVV = "0";
            validateValidPurchase.Expiration = "03-99";
            validateValidPurchase.DeliveryFirstName = "(*&^";
            validateValidPurchase.DeliveryLastName = "(*&%%^$";
            Func<int> validPurchase = () => { provider.ValidatePurchaseInputFields(validateValidPurchase); return 0; };
            validPurchase.Should().Throw<BadRequestException>();
        }

        [Fact]
        public void ValidatePurchaseInputFields_ThrowsBadRequestExceptionIfTheseInvalidFieldsAreReachedInLogic()
        {
            validateValidPurchase.BillingStreet2 = "()*&^ Test Ave.";
            validateValidPurchase.DeliveryStreet2 = "Random Invalid Street )(*^";
            validateValidPurchase.CardNumber = "123412341234123";
            validateValidPurchase.Expiration = "08/20";
            Func<int> validPurchase = () => { provider.ValidatePurchaseInputFields(validateValidPurchase); return 0; };
            validPurchase.Should().Throw<BadRequestException>();
        }

        [Fact]
        public void ValidatePurchaseInputFields_ThrowsBadRequestExceptionIfThereAreEmptyRequiredFields()
        {
            validateValidPurchase.BillingEmail = "";
            validateValidPurchase.BillingPhone = null;
            validateValidPurchase.BillingStreet = " ";
            validateValidPurchase.BillingCity = "";
            validateValidPurchase.BillingZip = null;
            validateValidPurchase.DeliveryStreet = "    ";
            validateValidPurchase.DeliveryCity = string.Empty;
            validateValidPurchase.DeliveryZip = string.Empty;
            validateValidPurchase.CardHolder = null;
            validateValidPurchase.CardNumber = "";
            validateValidPurchase.CVV = " ";
            validateValidPurchase.Expiration = null;
            validateValidPurchase.DeliveryFirstName = string.Empty;
            validateValidPurchase.DeliveryLastName = string.Empty;
            Func<int> validPurchase = () => { provider.ValidatePurchaseInputFields(validateValidPurchase); return 0; };
            validPurchase.Should().Throw<BadRequestException>();
        }

        [Fact]
        public async void CreatePurchaseAsync_WithValidPurchase_ReturnsExpectedPurchase()
        {
            var result = await provider.CreatePurchasesAsync(validateValidPurchase);
            result.Should().BeEquivalentTo(validateValidPurchase, options => options.ComparingByMembers<Data.Model.Purchase>());
        }
    }
}
