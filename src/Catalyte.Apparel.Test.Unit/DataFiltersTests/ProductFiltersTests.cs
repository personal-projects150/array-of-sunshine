﻿using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.SeedData;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Unit.DataFiltersTests
{
    public class ProductFiltersTests
    {
        private readonly ProductFactory _factory = new();

        // Default ProductFilterParams returns all products
        private readonly ProductFilterParams defaultProductFilterParams = new();
        private readonly Product testProduct;
        private readonly List<Product> testProducts;

        public ProductFiltersTests()
        {
            testProduct = _factory.CreateRandomProduct(1);
            testProducts = _factory.GenerateRandomProducts(4);
        }

        [Fact]
        public void WhereProductIdEquals_WithExistingProduct_ReturnsExpectedProduct()
        {
            // Arrange
            var product = new List<Product>() { testProduct };
            // Act
            var result = ProductFilters.WhereProductIdEquals(product.AsQueryable(), 1);

            // Assert
            result.Should().BeEquivalentTo(testProduct);
        }

        [Fact]
        public void ApplyProductFilters_WithDefaultFilters_ReturnsAllProducts()
        {
            // Arrange
            var products = testProducts.AsQueryable();
            // Act
            var result = products.ApplyProductFilters(defaultProductFilterParams);
            // Assert
            result.Should().BeEquivalentTo(products);
        }

        [Theory]
        [InlineData("brand")]
        [InlineData("category")]
        [InlineData("demographic")]
        [InlineData("price")]
        [InlineData("color")]
        [InlineData("material")]
        public void ApplyProductFilters_WithNoMatch_ReturnsEmptyCollection(string filterBy)
        {
            // Arrange
            var products = testProducts.AsQueryable();
            var unMatchingValue = "F#O)958qZ-!";
            var filterParams = new ProductFilterParams()
            {
                FilterBy = new[] { filterBy },
                Brands = new[] { unMatchingValue },
                Categories = new[] { unMatchingValue },
                Demographics = new[] { unMatchingValue },
                MinPrices = new[] { double.MaxValue },
                MaxPrices = new[] { 0.0 },
                Colors = new[] { unMatchingValue },
                Materials = new[] { unMatchingValue }
            };

            // Act
            var result = products.ApplyProductFilters(filterParams);

            // Assert
            result.Should().BeEquivalentTo(Array.Empty<Product>());
        }

        [Fact]
        public void FilterBySingleKey_WithSingleValue_ReturnsExpectedProducts()
        {
            var products = testProducts.AsQueryable();
            var filterParams = new ProductFilterParams()
            {
                FilterBy = new[] { "demographic" },
                Demographics = new[] { "men" }
            };

            var result = products.ApplyProductFilters(filterParams);
            var complement = products.Except(result);
            var union = result.Union(complement);

            Assert.All(result, p => Assert.Equal("men", p.Demographic.ToLower()));
            Assert.All(complement, p => Assert.NotEqual("men", p.Demographic.ToLower()));

            union.Should().BeEquivalentTo(products);
        }

        [Theory]
        [InlineData("brand", "Nike")]
        [InlineData("category", "golf")]
        [InlineData("demographic", "women")]
        [InlineData("material", "Polypropylene")]
        public void ApplyProductFilters_WithSingleValue_ReturnsExpectedResults(string filterBy, string value)
        {
            // Arrange
            var products = testProducts;
            var productsQueryable = products.AsQueryable();

            var filterParams = new ProductFilterParams()
            {
                FilterBy = new[] { filterBy },
                Brands = new[] { value },
                Categories = new[] { value },
                Demographics = new[] { value },
                Materials = new[] { value }
            };
            
            // Act
            var result = productsQueryable.ApplyProductFilters(filterParams);

            // Assert
            foreach (var p in result)
            {
                switch(filterBy)
                {
                    case "brand":
                        Assert.Contains(p.Brand.ToLower(), filterParams.Brands);
                        break;
                    case "category":
                        Assert.Contains(p.Category.ToLower(), filterParams.Categories);
                        break;
                    case "demographic":
                        Assert.Contains(p.Demographic.ToLower(), filterParams.Demographics);
                        break;
                    case "material":
                        Assert.Contains(p.Material.ToLower(), filterParams.Materials);
                        break;
                }
            }
        }

        [Fact]
        public void FilterByColor_WithSingleValue_ReturnsExpectedProducts()
        {
            // Arrange
            var products = testProducts;
            string[] colors = new[] { "red", "yellow", "orange", "green", "blue", "colorOutOfSpace" };
            foreach (var product in products)
            {
                Random random = new();
                product.PrimaryColorCode = colors[random.Next(colors.Length)];
                product.SecondaryColorCode = colors[random.Next(colors.Length)];
            }

            var filterParams = new ProductFilterParams()
            {
                FilterBy = new[] { "color" },
                Colors = new[] { "red" }
            };
            var productsQueryable = products.AsQueryable();

            // Act
            var result = productsQueryable.ApplyProductFilters(filterParams);
            var complement = products.Except(result);
            var union = result.Union(complement);

            // Assert
            Assert.All(result, p => Assert.True( p.PrimaryColorCode == "red" || p.SecondaryColorCode == "red"));
            Assert.All(complement, p => Assert.True(p.PrimaryColorCode != "red" && p.SecondaryColorCode != "red"));
            union.Should().BeEquivalentTo(products);
        }

        [Fact]
        public void FilterByPrice_WithMinAndMaxPrice_ReturnsExpectedProducts()
        {
            // Arrange
            var products = testProducts;
            
            var filterParams = new ProductFilterParams()
            {
                FilterBy = new [] { "price"},
                MinPrices = new[] { 100.0 },
                MaxPrices = new[] { 600.0 }
            };
            var productsQueryable = products.AsQueryable();

            // Act
            var result = productsQueryable.ApplyProductFilters(filterParams);

            // Assert
            Assert.All(result, p => Assert.True(p.Price <= 600 && p.Price >= 100));
        }

        [Fact]
        public void FilterBySingleKey_WithMultipleValues_ReturnsExpectedProducts()
        {
            var products = testProducts.AsQueryable();
            var filterParams = new ProductFilterParams()
            {
                FilterBy = new[] { "category" },
                Categories = new[] { "Soccer", "Golf" }
            };

            var result = products.ApplyProductFilters(filterParams);
            var complement = products.Except(result);
            var union = result.Union(complement);

            Assert.All(result, p => Assert.True(p.Category == "Soccer" || p.Category == "Golf"));
            Assert.All(complement, p => Assert.True(p.Category != "Soccer" && p.Category != "Golf"));

            union.Should().BeEquivalentTo(products);
        }

        [Fact]
        public void FilterByMultipleKeys_ReturnsExpectedProducts()
        {
            // Arrange
            var products = testProducts.AsQueryable();
            var filterParams = new ProductFilterParams()
            {
                FilterBy = new[] { "category", "demographic" },
                Categories = new[] { "golf" },
                Demographics = new[] { "men" }
            };

            // Act
            var result = products.ApplyProductFilters(filterParams);
            var complement = products.Except(result);
            var union = result.Union(complement);

            // Assert
            Assert.All(result, product => Assert.True(product.Category.ToLower() == "golf" && product.Demographic.ToLower() == "men"));
            Assert.All(complement, product => Assert.True(product.Category.ToLower() != "golf" || product.Demographic.ToLower() != "men"));
            union.Should().BeEquivalentTo(products);
        }
    }
}
