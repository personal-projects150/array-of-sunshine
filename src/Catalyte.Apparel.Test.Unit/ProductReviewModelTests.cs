﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Model;
using Xunit;

namespace Catalyte.Apparel.Test.Unit
{
    public class ProductReviewModelTests
    {
        private IEnumerable<ValidationResult> ValidateModel(object model)
        {
            var validationResults = new List<ValidationResult>();
            var ctx = new ValidationContext(model, null, null);
            Validator.TryValidateObject(model, ctx, validationResults, true);
            return validationResults;
        }

        [Fact]
        public void ModelCreationWithRatingOutOfRange()
        {
            // Arrange
            var productReview = new ProductReview()
            {
                Title = "Hello world",
                Rating = 42,
                DatePosted = DateTime.Now,
                Comment = "This is a comment."
            };
            // Act
            var results = ValidateModel(productReview);
            // Assert
            Assert.Contains(results, result => result.ErrorMessage == "The field Rating must be between 1 and 5.");
        }
    }
}
