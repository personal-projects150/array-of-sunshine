﻿using System.Threading.Tasks;
using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the product review repository.
    /// </summary>
    public class ProductReviewRepository : IProductReviewRepository
    {
        private readonly ILogger<ProductReviewRepository> _logger;
        private readonly IApparelCtx _ctx;

        public ProductReviewRepository(ILogger<ProductReviewRepository> logger, IApparelCtx ctx)
        {
            _logger = logger;
            _ctx = ctx;
        }

        public async Task<ProductReview> GetProductReviewByIdAsync(int productReviewId)
        {
            return await _ctx.ProductReviews
                .AsNoTracking()
                .Where(pr => pr.Id == productReviewId)
                .AsQueryable()
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<ProductReview>> GetAllProductReviewsAsync()
        {
            return await _ctx.ProductReviews
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<ProductReview> CreateProductReviewAsync(ProductReview productReview)
        {
            _ctx.ProductReviews.Add(productReview);
            await _ctx.SaveChangesAsync();
            return productReview;
        }

        public async Task<IEnumerable<ProductReview>> GetProductReviewsByProductIdAsync(int productId)
        {
            return await _ctx.ProductReviews
                .AsNoTracking()
                .Where(pr => pr.ProductId == productId)
                .AsQueryable()
                .ToListAsync();
        }
        public async Task<ProductReview> DeleteProductReviewByIdAsync(int productReviewId)
        {
            var productReview = await _ctx.ProductReviews
                .AsNoTracking()
                .Where(pr => pr.Id == productReviewId)
                .SingleOrDefaultAsync();
            _ctx.ProductReviews.Remove(productReview);
            await _ctx.SaveChangesAsync();
            return productReview;
        }

        public async Task<ProductReview> UpdateProductReviewAsync(ProductReview productReview)
        {
            _ctx.ProductReviews.Update(productReview);
            await _ctx.SaveChangesAsync();

            return productReview;
        }
    }
}
