﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the product repository.
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        private readonly IApparelCtx _ctx;

        public ProductRepository(IApparelCtx ctx)
        {
            _ctx = ctx;
        }

        public async Task<Product> GetProductByIdAsync(int productId)
        {
            return await _ctx.Products
                .AsNoTracking()
                .WhereProductIdEquals(productId)
                .Include(product => product.Reviews)
                .SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<Product>> GetProductsAsync(ProductFilterParams filterParams)
        {
            return await _ctx.Products
                .AsNoTracking()
                .ApplyProductFilters(filterParams)
                .Include(product => product.Reviews)
                .ToListAsync();
        }

        public async Task<IEnumerable<Product>> GetActiveFilteredPagedProductsAsync(PaginationParams @params, ProductFilterParams filterParams)
        {
            return await _ctx.Products
                .AsNoTracking()
                .ApplyProductFilters(filterParams)
                .Where(product => product.Active)
                .OrderBy(p => p.Id)
                .Skip((@params.Page) * @params.ItemsPerPage)
                .Take(@params.ItemsPerPage)
                .Include(product => product.Reviews)
                .ToListAsync();
        }

        public async Task<Product> CreateProductAsync(Product product)
        {
            _ctx.Products.Add(product);
            await _ctx.SaveChangesAsync();

            return product;
        }

        public async Task<Product> DeleteProductByIdAsync(int productId)
        {
            var product = await _ctx.Products.AsNoTracking().WhereProductIdEquals(productId).SingleOrDefaultAsync();
            _ctx.Products.Remove(product);
            await _ctx.SaveChangesAsync();
            return product;
        }

        public async Task<Product> ToggleProductActiveStateByIdAsync(int productId)
        {
            var product = await _ctx.Products
                .AsNoTracking()
                .WhereProductIdEquals(productId)
                .SingleOrDefaultAsync();
            product.Active = !product.Active;
            _ctx.Products.Update(product);
            await _ctx.SaveChangesAsync();
            return product;
        }


        public async Task<Product> UpdateProductByIdAsync(Product newProduct, int productId)
        {
            var product = await _ctx.Products.AsNoTracking().WhereProductIdEquals(productId).SingleOrDefaultAsync();

            product.Id = product.Id;
            product.DateCreated = product.DateCreated;
            product.DateModified = newProduct.DateModified;
            product.Active = newProduct.Active;
            if (newProduct.Name != null)
            {
            product.Name = newProduct.Name;
            } else
            {
                product.Name = product.Name;
            }
            if (newProduct.Sku != null)
            {
                product.Sku = newProduct.Sku;
            }
            else
            {
                product.Sku = product.Sku;
            }
            if (newProduct.Description != null)
            {
                product.Description = newProduct.Description;
            }
            else
            {
                product.Description = product.Description;
            }
            if (newProduct.Demographic != null)
            {
                product.Demographic = newProduct.Demographic;
            }
            else
            {
                product.Demographic = product.Demographic;
            }
            if (newProduct.Category != null)
            {
                product.Category = newProduct.Category;
            }
            else
            {
                product.Category = product.Category;
            }
            if (newProduct.Type != null)
            {
                product.Type = newProduct.Type;
            }
            else
            {
                product.Type = product.Type;
            }
            if (newProduct.ReleaseDate != System.DateTime.Parse("0001-01-01T00:00:00"))
            {
                product.ReleaseDate = newProduct.ReleaseDate;
            }
            else
            {
                product.ReleaseDate = product.ReleaseDate;
            }
            if (newProduct.PrimaryColorCode != null)
            {
                product.PrimaryColorCode = newProduct.PrimaryColorCode;
            }
            else
            {
                product.PrimaryColorCode = product.PrimaryColorCode;
            }
            if (newProduct.SecondaryColorCode != null)
            {
                product.SecondaryColorCode = newProduct.SecondaryColorCode;
            }
            else
            {
                product.SecondaryColorCode = product.SecondaryColorCode;
            }
            if (newProduct.StyleNumber != null)
            {
                product.StyleNumber = newProduct.StyleNumber;
            }
            else
            {
                product.StyleNumber = product.StyleNumber;
            }
            if (newProduct.GlobalProductCode != null)
            {
                product.GlobalProductCode = newProduct.GlobalProductCode;
            }
            else
            {
                product.GlobalProductCode = product.GlobalProductCode;
            }
            product.Active = newProduct.Active;
            if (newProduct.Brand != null)
            {
                product.Brand = newProduct.Brand;
            }
            else
            {
                product.Brand = product.Brand;
            }
            if (newProduct.ImageSrc != null)
            {
                product.ImageSrc = newProduct.ImageSrc;
            }
            else
            {
                product.ImageSrc = product.ImageSrc;
            }
            if (newProduct.Material != null)
            {
                product.Material = newProduct.Material;
            }
            else
            {
                product.Material = product.Material;
            }
            if (newProduct.Price != 0)
            {
                product.Price = newProduct.Price;
            }
            else
            {
                product.Price = product.Price;
            }
            if (newProduct.Quantity != 0)
            {
                product.Quantity = newProduct.Quantity;
            }
            else
            {
                product.Quantity = product.Quantity;
            }
            product.DateModified = System.DateTime.Now;

            _ctx.Products.Update(product);
            await _ctx.SaveChangesAsync();
            return product;
        }
    }
}
