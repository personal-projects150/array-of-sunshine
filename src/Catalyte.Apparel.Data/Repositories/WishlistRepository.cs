﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the product review repository.
    /// </summary>
    public class WishlistRepository : IWishlistRepository
    {
        private readonly ILogger<WishlistRepository> _logger;
        private readonly IApparelCtx _ctx;

        public WishlistRepository(ILogger<WishlistRepository> logger, IApparelCtx ctx)
        {
            _logger = logger;
            _ctx = ctx;
        }

        public async Task<WishlistItem> AddToWishlistAsync(WishlistItem wishlistItem)
        {
            _ctx.WishlistItems.Add(wishlistItem);
            await _ctx.SaveChangesAsync();
            return wishlistItem;
        }

        public async Task<List<WishlistItem>> GetWishlistByUserIdAsync(int userId)
        {
            return await _ctx.WishlistItems
            .AsNoTracking()
            .Where(wi => wi.UserId == userId)
            .ToListAsync();
        }
        public async Task<WishlistItem> DeleteWishlistItemByIdAsync(int wishlistItemId)
        {
            var wishlistItem = await _ctx.WishlistItems
                .AsNoTracking()
                .Where(wi => wi.Id == wishlistItemId)
                .SingleOrDefaultAsync();
            _ctx.WishlistItems.Remove(wishlistItem);
            await _ctx.SaveChangesAsync();
            return wishlistItem;
        }
    }
}