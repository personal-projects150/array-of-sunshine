﻿using Catalyte.Apparel.Data.Context;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Repositories
{
    /// <summary>
    /// This class handles methods for making requests to the promo code repository.
    /// </summary>
    public class PromoCodeRepository : IPromoCodeRepository
    {
        private readonly ILogger<PromoCodeRepository> _logger;
        private readonly IApparelCtx _ctx;

        public PromoCodeRepository(ILogger<PromoCodeRepository> logger, IApparelCtx ctx)
        {
            _logger = logger;
            _ctx = ctx;
        }


        public async Task<PromoCode> CreatePromoCodeAsync(PromoCode promoCode)
        {
            await _ctx.PromoCodes.AddAsync(promoCode);
            await _ctx.SaveChangesAsync();

            return promoCode;
        }
    }
}
