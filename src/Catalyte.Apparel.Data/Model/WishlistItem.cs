﻿using System;

namespace Catalyte.Apparel.Data.Model
{
    /// <summary>
    /// Describes wishlist item
    /// </summary>
    public class WishlistItem : BaseEntity
    {
        public int UserId { get; set; }

        public int ProductId { get; set; }

        public string ProductName { get; set; }
    }
}