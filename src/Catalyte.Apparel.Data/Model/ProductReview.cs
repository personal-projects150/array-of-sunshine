﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Catalyte.Apparel.Data.Model
{/// <summary>
 /// This class is a representation of a product review.
 /// </summary>
    public class ProductReview : BaseEntity
    {
        [DataType(DataType.Text)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The title cannot be empty.")]
        [StringLength(100)]
        public string Title { get; set; }

        [Range(1, 5)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The rating cannot be empty.")]
        public Nullable<double> Rating { get; set; }

        [DataType(DataType.Date)]
        public DateTime DatePosted { get; set; }

        [StringLength(2000)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "The comment cannot be empty.")]
        public string Comment { get; set; }

        public int? ProductId { get; set; }

        public Product Product { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
