﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Catalyte.Apparel.Data.Model
{
    public record ProductFilterParams
    {
#nullable enable

        private static readonly double _minPrice = 0;
        private static readonly double _maxPrice = double.MaxValue;
        private string[]? _filterBy = null;
        private string[]? _brands = null;
        private string[]? _categories = null;
        private string[]? _demographics = null;
        private string[]? _colors = null;
        private string[]? _materials = null;

        public string[]? FilterBy
        { 
            get { return _filterBy; }
            set => _filterBy = value.Select(str => str.ToLower()).ToArray();
        }

        [ModelBinder(Name = "brand")]
        public string[]? Brands
        {
            get { return _brands; }
            set => _brands = value.Select(str => str.ToLower()).ToArray();
        }

        [ModelBinder(Name = "category")]
        public string[]? Categories
        {
            get { return _categories; }
            set => _categories = value.Select(str => str.ToLower()).ToArray();
        }

        [ModelBinder(Name = "demographic")]
        public string[]? Demographics
        {
            get { return _demographics; }
            set => _demographics = value.Select(str => str.ToLower()).ToArray();
        }

        [ModelBinder(Name = "minPrice")]
        public double[]? MinPrices { get; set; } = new[] { _minPrice };

        [ModelBinder(Name = "maxPrice")]
        public double[]? MaxPrices { get; set; } = new[] { _maxPrice };

        [ModelBinder(Name = "color")]
        public string[]? Colors
        {
            get { return _colors; }
            set => _colors = value.Select(str => str.ToLower()).ToArray();
        }

        [ModelBinder(Name = "material")]
        public string[]? Materials
        {
            get { return _materials; }
            set => _materials = value.Select(str => str.ToLower()).ToArray();
        }

    }
}
