﻿namespace Catalyte.Apparel.Data.Model
{
    /// <summary>
    /// Describes a promo code for transaction.
    /// </summary>
    public class PromoCode : BaseEntity
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Type { get; set; }

        public string Rate { get; set; }
    }
}
