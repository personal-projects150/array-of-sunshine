﻿using System.Threading.Tasks;
using Catalyte.Apparel.Data.Model;
using System.Collections.Generic;

namespace Catalyte.Apparel.Data.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for product review repository methods.
    /// </summary>
    public interface IProductReviewRepository
    {
        Task<ProductReview> GetProductReviewByIdAsync(int productReviewId);
        Task<IEnumerable<ProductReview>> GetAllProductReviewsAsync();
        Task<ProductReview> CreateProductReviewAsync(ProductReview productReview);
        Task<IEnumerable<ProductReview>> GetProductReviewsByProductIdAsync(int productId);
		Task <ProductReview> DeleteProductReviewByIdAsync(int productReviewId);
        Task<ProductReview> UpdateProductReviewAsync(ProductReview productReview);
    }
}
