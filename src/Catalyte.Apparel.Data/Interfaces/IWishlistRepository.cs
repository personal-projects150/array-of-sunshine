﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Model;

namespace Catalyte.Apparel.Data.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for wishlist repository methods.
    /// </summary>
    public interface IWishlistRepository
    {
        Task<WishlistItem> AddToWishlistAsync(WishlistItem favoriteProduct);
        Task<List<WishlistItem>> GetWishlistByUserIdAsync(int userId);
        Task<WishlistItem> DeleteWishlistItemByIdAsync(int wishlistItemId);

    }
}
