﻿using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for product repository methods.
    /// </summary>
    public interface IProductRepository
    {
        Task<Product> GetProductByIdAsync(int productId);
        Task<IEnumerable<Product>> GetProductsAsync(ProductFilterParams filterParams);

        Task<IEnumerable<Product>> GetActiveFilteredPagedProductsAsync(PaginationParams @params, ProductFilterParams filterParams);

        Task<Product> CreateProductAsync(Product newProduct);

        Task<Product> DeleteProductByIdAsync(int productId);
        Task<Product> UpdateProductByIdAsync(Product newProduct, int productId);

        Task<Product> ToggleProductActiveStateByIdAsync(int productId);
    }
}
