﻿using Catalyte.Apparel.Data.Model;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Data.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for promo code repository methods.
    /// </summary>
    public interface IPromoCodeRepository
    {

        Task<PromoCode> CreatePromoCodeAsync(PromoCode promoCode);
    }
}
