﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;

namespace Catalyte.Apparel.Data.SeedData
{
    public class ProductReviewFactory
    {
        private readonly List<Product> products;
        private readonly List<User> users;
        private readonly Random random = new Random();

        public ProductReviewFactory(List<Product> products, List<User> users)
        {
            this.products = products;
            this.users = users;
        }

        private string GetRandomTitle()
        {
            List<string> titles = new List<string>()
            {
                "Helps me look my best at the gym",
                "The perfect apparel for long walks to Mordor",
                "Smells of elderberries",
                "I'm the best-dressed boxer in the ring thanks to this",
                "Everyone in House Ravenclaw is wearing these to the game. We look sharp on the field and we know it."
            };
            return titles[random.Next(titles.Count)];
        }

        private int GetRandomRating()
        {
            return random.Next(1, 6);
        }

        private DateTime GetRandomDate()
        {
            DateTime date = DateTime.Now;
            return date.AddDays(-1 * random.Next(1000));
        }

        private string GetRandomComment()
        {
            string lipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget facilisis magna, a convallis enim. Phasellus fermentum, justo et laoreet iaculis, orci augue malesuada quam, in porta enim est non ex. Aliquam imperdiet in dolor a cursus. Proin lobortis sagittis luctus. Ut vestibulum, augue a aliquet venenatis, risus metus varius orci, eget sodales purus nisi a ante. Nam sapien nulla, accumsan et mattis vitae, facilisis eget risus. Phasellus mollis lectus tincidunt, rhoncus metus ut, dictum nisi. Donec dui velit, vestibulum ut ullamcorper et, cursus vitae elit. Interdum et malesuada fames ac ante ipsum primis in faucibus. Aenean eu gravida felis. Cras elementum, nunc et accumsan ornare, odio massa elementum elit, sit amet feugiat nulla diam luctus nulla. Sed mattis pulvinar ex, in tristique mi posuere eget. Vestibulum congue felis laoreet ex pretium tempor.Mauris lacinia venenatis libero varius molestie. Curabitur vitae leo molestie, sagittis velit vitae, pellentesque ex.Phasellus a auctor tellus. Vestibulum in placerat quam. Praesent euismod neque laoreet purus consectetur convallis.Nullam lectus lorem, ullamcorper eu velit vitae, fringilla viverra diam. Nulla id felis urna. Maecenas eget ipsum volutpat, tincidunt dolor nec, mollis erat. Donec vitae finibus justo. Nullam fringilla hendrerit magna id semper. Aliquam at imperdiet ipsum, at rutrum ante. Cras nec purus quis nulla tempus convallis.Proin in ipsum nulla. Suspendisse consequat arcu sit amet blandit pellentesque.Sed auctor ipsum vitae augue sagittis, ac rhoncus sem posuere.Nulla purus tellus, gravida finibus blandit ac, scelerisque vitae lorem. Fusce feugiat, orci vel maximus rhoncus, nunc turpis aliquet ex, sed pretium diam massa non enim.In id laoreet eros. Sed vulputate ac tortor at porta. Sed bibendum sollicitudin tempor. Nulla laoreet imperdiet nibh, sed dignissim ligula. Ut eleifend justo eu convallis ultricies. Nulla tempus massa vel mauris commodo, nec convallis tellus mattis.Vivamus aenean.";
            int start = lipsum.IndexOf(". ", random.Next(0, 500)) + 2;
            int end = -1;
            while (end < 0 || end - start < 100)
            {
                end = lipsum.LastIndexOf(".", random.Next(start, lipsum.Length));
            }
            return lipsum.Substring(start, end - start + 1);
        }
        private User GetRandomUser()
        {
            return users[random.Next(users.Count)];
        }

        public ProductReview CreateRandomReview(int id, Product product)
        {
            var date = GetRandomDate();
            return new ProductReview()
            {
                Id = id,
                UserId = GetRandomUser().Id,
                ProductId = product.Id,
                Title = GetRandomTitle(),
                Rating = GetRandomRating(),
                DatePosted = date,
                DateCreated = date,
                DateModified = date,
                Comment = GetRandomComment()
            };
        }

        public List<ProductReview> GenerateRandomReviews()
        {
            var counter = 1;
            var reviews = new List<ProductReview>();
            foreach (Product product in products)
            {
                var numberOfReviews = random.Next(0, 5);
                for (int i = 0; i < numberOfReviews; i++)
                {
                    var review = CreateRandomReview(counter++, product);
                    reviews.Add(review);
                }
            }

            return reviews;
        }
    }
}
