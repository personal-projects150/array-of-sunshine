﻿using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.SeedData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Catalyte.Apparel.Data.Context
{
    public static class Extensions
    {
        /// <summary>
        /// Produces a set of seed data to insert into the database on startup.
        /// </summary>
        /// <param name="modelBuilder">Used to build model base DbContext.</param>
        public static void SeedData(this ModelBuilder modelBuilder)
        {
            var productFactory = new ProductFactory();
            var products = productFactory.GenerateRandomProducts(1000);

            modelBuilder.Entity<Product>().HasData(products);
            var users = new List<User>()
            {
                new User()
                {
                    Id = 1,
                    Email = "ajokerst@catalyte.io",
                    FirstName = "Abbie",
                    LastName = "Jokerst",
                    Street = "123 Abbey Rd",
                    City = "Sunshine",
                    State = "Colorado",
                    Zip = "12345"
                },
                    new User()
                {
                    Id = 2,
                    Email = "ehuffman@catalyte.io",
                    FirstName = "Emily",
                    LastName = "Huffman",
                    Street = "2816 Tanagrine Dr",
                    City = "North Las Vegas",
                    State = "Nevada",
                    Zip = "89084"
                },
                    new User()
                {
                    Id = 3,
                    Email = "kmacgregor@catalyte.io",
                    FirstName = "Kyle",
                    LastName = "MacGregor",
                    Street = "21 Bag End Lane",
                    City = "Hobbiton",
                    State = "New Hampshire",
                    Zip = "46586"
                },
                    new User()
                {
                    Id = 4,
                    Email = "scasavant@catalyte.io",
                    FirstName = "Samuel",
                    LastName = "Casavant",
                    Street = "65 Buckeye St",
                    City = "Columbus",
                    State = "Ohio",
                    Zip = "11224"
                },
                    new User()
                {
                    Id = 5,
                    Email = "dcastagno@catalyte.io",
                    FirstName = "Devon",
                    LastName = "Castagno",
                    Street = "22 Easy Street",
                    City = "Portland",
                    State = "Oregon",
                    Zip = "65478"
                },
                    new User()
                {
                    Id = 6,
                    Email = "aparson@catalyte.io",
                    FirstName = "Arcadia",
                    LastName = "Parson",
                    Street = "258 Tropical Paradise Rd",
                    City = "Guamville",
                    State = "Guam",
                    Zip = "89456"
                },
                    new User()
                {
                    Id = 7,
                    Email = "khawkins@catalyte.io",
                    FirstName = "Kaleigh",
                    LastName = "Hawkins",
                    Street = "62 Coco Nuts Lane",
                    City = "Oahu",
                    State = "Hawaii",
                    Zip = "36945"
                },
                    new User()
                {
                    Id = 8,
                    Email = "tpatel@catalyte.io",
                    FirstName = "Tejal",
                    LastName = "Patel",
                    Street = "22 Sweet Bourbon Rd",
                    City = "Louisville",
                    State = "Kentucky",
                    Zip = "45782"
                },
                    new User()
                {
                    Id = 9,
                    Email = "agoodrum@catalyte.io",
                    FirstName = "Allison",
                    LastName = "Goodrum",
                    Street = "786 Punderful Place",
                    City = "Ft Drum",
                    State = "New York",
                    Zip = "78224"
                },
                    new User()
                {
                    Id = 10,
                    Email = "aseitz@catalyte.io",
                    FirstName = "Ariel",
                    LastName = "Seitz",
                    Street = "754 Truly Drive",
                    City = "Anonymous",
                    State = "California",
                    Zip = "15973"
                },
                    new User()
                {
                    Id = 13,
                    Email = "jchristman@catalyte.io",
                    FirstName = "Jaron",
                    LastName = "Christman",
                    Street = "756 Visor Hat Circle",
                    City = "Mustachio",
                    State = "Oregon",
                    Zip = "89456"
                }
            };

            modelBuilder.Entity<User>().HasData(users);

            var reviewFactory = new ProductReviewFactory(products, users);
            var reviews = reviewFactory.GenerateRandomReviews();

            modelBuilder.Entity<ProductReview>().HasData(reviews);

            var lineItem = new LineItem()
            {
                Id = 1,
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                ProductId = 1,
                ProductName = "Fashionable Football Headband",
                Quantity = 1,
                PurchaseId = 1
            };

            modelBuilder.Entity<LineItem>().HasData(lineItem);

            var purchase = new Purchase()
            {
                Id = 1,
                BillingCity = "Atlanta",
                BillingEmail = "customer@home.com",
                BillingPhone = "(714) 345-8765",
                BillingState = "GA",
                BillingStreet = "123 Main",
                BillingStreet2 = "Apt A",
                BillingZip = "31675",
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                DeliveryCity = "Birmingham",
                DeliveryState = "AL",
                DeliveryStreet = "123 Hickley",
                DeliveryZip = "43690",
                DeliveryFirstName = "Max",
                DeliveryLastName = "Space",
                CardHolder = "Max Perkins",
                CardNumber = "1435678998761234",
                Expiration = "11/21",
                CVV = "456",
                PurchaseTotal = 750.50,
                OrderDate = new DateTime(2021, 5, 4)
            };

            modelBuilder.Entity<Purchase>().HasData(purchase);
        }
    }
}

