﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Catalyte.Apparel.Data.SeedData
{
    /// <summary>
    /// This class provides tools for generating random products.
    /// </summary>
    public class ProductFactory
    {
        readonly Random _rand = new();

        public readonly List<string> _colors = new()
        {
            "#000000", // white
            "#ffffff", // black
            "#39add1", // light blue
            "#3079ab", // dark blue
            "#c25975", // mauve
            "#e15258", // red
            "#f9845b", // orange
            "#838cc7", // lavender
            "#7d669e", // purple
            "#53bbb4", // aqua
            "#51b46d", // green
            "#e0ab18", // mustard
            "#637a91", // dark gray
            "#f092b0", // pink
            "#b7c0c7"  // light gray
        };

        public readonly List<string> _demographics = new()
        {
            "Men",
            "Women",
            "Kids"
        };

        public readonly List<string> _categories = new()

        {
            "Golf",
            "Soccer",
            "Basketball",
            "Hockey",
            "Football",
            "Running",
            "Baseball",
            "Skateboarding",
            "Boxing",
            "Weightlifting"
        };

        public readonly List<string> _adjectives = new()
        {
            "Lightweight",
            "Slim",
            "Shock Absorbing",
            "Exotic",
            "Elastic",
            "Fashionable",
            "Trendy",
            "Next Gen",
            "Colorful",
            "Comfortable",
            "Water Resistant",
            "Wicking",
            "Heavy Duty"
        };

        public readonly List<string> _types = new()
        {
            "Pant",
            "Short",
            "Shoe",
            "Glove",
            "Jacket",
            "Tank Top",
            "Sock",
            "Sunglasses",
            "Hat",
            "Helmet",
            "Belt",
            "Visor",
            "Shin Guard",
            "Elbow Pad",
            "Headband",
            "Wristband",
            "Hoodie",
            "Flip Flop",
            "Pool Noodle"
        };

        public readonly List<string> _skuMods = new()
        {
            "Blue",
            "Red",
            "KJ",
            "SM",
            "RD",
            "LRG",
            "SM"
        };

        public readonly List<string> _brands = new()
        {
            "Nike",
            "Adidas",
            "Brooks",
            "Under Armour",
            "Champion",
            "Puma",
            "New Balance",
            "Asics",
            "Columbia",
            "Lululemon",
            "Reebok"
        };

        public readonly List<string> _materials = new()
        {
            "Cotton",
            "Microfiber",
            "Bamboo Fiber",
            "Gore-Tex",
            "Polyester",
            "Nylon",
            "Spandex",
            "Polypropylene",
            "Tencel",
            "Wool",
            "Neoprene"
        };

        /// <summary>
        /// Generates a randomized product SKU.
        /// </summary>
        /// <returns>A SKU string.</returns>
        public string GetRandomSku()
        {
            var builder = new StringBuilder();
            builder.Append(RandomString(3));
            builder.Append('-');
            builder.Append(RandomString(3));
            builder.Append('-');
            builder.Append(_skuMods[_rand.Next(0, 6)]);

            return builder.ToString().ToUpper();
        }

        /// <summary>
        /// Returns a random demographic from the list of demographics.
        /// </summary>
        /// <returns>A demographic string.</returns>
        private string GetDemographic()
        {
            var demographic = _demographics[_rand.Next(0, 3)];
            return demographic;
        }

        /// <summary>
        /// Generates a random product offering id.
        /// </summary>
        /// <returns>A product offering string.</returns>
        public string GetRandomProductId()
        {
            var productId = "po-" + RandomString(7);
            return productId;
        }

        /// <summary>
        /// Generates a random style code.
        /// </summary>
        /// <returns>A style code string.</returns>
        private string GetStyleCode()
        {
            var styleCode = "sc" + RandomString(5);
            return styleCode;
        }

        /// <summary>
        /// Generates an image source.
        /// </summary>
        /// <returns>A url displayed as a string.</returns>
        private string GetImageSrc()
        {
            var imgSrc = "https://source.unsplash.com/b_S4jZlgtLo";
            return imgSrc;
        }

        /// <summary>
        /// Generates a random date between Jan 1, 2000 and Dec 31, 2050.
        /// </summary>
        /// <returns>A random date between Jan 1, 2000 and Dec 31, 2050.</returns>
        private Random gen = new Random();
        DateTime RandomDay()
        {
            DateTime start = new DateTime(2000, 1, 1);
            DateTime end = new DateTime(2050, 12, 31);
            int range = (end - start).Days;
            return start.AddDays(gen.Next(range));
        }

        /// <summary>
        /// Generates a number of random products based on input.
        /// </summary>
        /// <param name="numberOfProducts">The number of random products to generate.</param>
        /// <returns>A list of random products.</returns>
        public List<Product> GenerateRandomProducts(int numberOfProducts)
        {

            var productList = new List<Product>();

            for (var i = 0; i < numberOfProducts; i++)
            {
                productList.Add(CreateRandomProduct(i + 1));
            }

            return productList;
        }

        /// <summary>
        /// Uses random generators to build a products.
        /// </summary>
        /// <param name="id">ID to assign to the product.</param>
        /// <returns>A randomly generated product.</returns>
        public Product CreateRandomProduct(int id)
        {
            var Demographic = GetDemographic();
            var Category = _categories[_rand.Next(0, 10)];
            var Type = _types[_rand.Next(0, 19)];
            var Adjective = _adjectives[_rand.Next(0, 13)];

            return new Product
            {
                Id = id,
                PrimaryColorCode = _colors[_rand.Next(0, 15)],
                SecondaryColorCode = _colors[_rand.Next(0, 15)],
                Type = Type,
                Category = Category,
                Demographic = Demographic,
                Sku = GetRandomSku(),
                Brand = _brands[_rand.Next(0, 11)],
                Material = _materials[_rand.Next(0, 11)],
                Price = Math.Round(_rand.NextDouble() * ((500 - 0) + 1), 2),
                Quantity = _rand.Next(0, 1001),
                GlobalProductCode = GetRandomProductId(),
                ImageSrc = GetImageSrc(),
                StyleNumber = GetStyleCode(),
                ReleaseDate = RandomDay(),
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                Active = _rand.NextDouble() >= 0.5,
                Name = $"{Adjective} {Category} {Type}",
                Description = $"{Adjective} {Category} {Type} made for motivated {Demographic}."
            };
        }

        public string RandomString(int size, bool lowerCase = false)
        {

            // ** Learning moment **
            // Code From
            // https://www.c-sharpcorner.com/article/generating-random-number-and-string-in-C-Sharp/

            // ** Learning moment **
            // Always use a string builder when concatenating more than a couple of strings.
            // Why? https://www.geeksforgeeks.org/c-sharp-string-vs-stringbuilder/
            var builder = new StringBuilder(size);

            // Unicode/ASCII Letters are divided into two blocks
            // (Letters 65–90 / 97–122):
            // The first group containing the uppercase letters and
            // the second group containing the lowercase.  

            // char is a single Unicode character  
            char offset = lowerCase ? 'a' : 'A';
            const int lettersOffset = 26; // A...Z or a..z: length=26  

            for (var i = 0; i < size; i++)
            {
                // ** Learning moment **
                // Because 'char' is a reserved word you can put '@' at the beginning to allow
                // its use as a variable name.  You could do the same thing with 'class'
                var @char = (char)_rand.Next(offset, offset + lettersOffset);
                builder.Append(@char);
            }

            return lowerCase ? builder.ToString().ToLower() : builder.ToString();
        }

        public class ProductList
        {
        }
    }
}

