﻿using System;
namespace Catalyte.Apparel.Data.Filters
{
    public class PaginationParams
    {
        private const int _maxItemsPerPage = 20;
        private int itemsPerPage;

        public int Page { get; set; }
        public int ItemsPerPage
        {
            get => itemsPerPage;
            set => itemsPerPage = value > _maxItemsPerPage ? _maxItemsPerPage : value;
        }
    }
}
