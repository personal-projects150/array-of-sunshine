﻿using Catalyte.Apparel.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Catalyte.Apparel.Data.Filters
{
    /// <summary>
    /// Filter collection for product context queries.
    /// </summary>
    public static class ProductFilters
    {
        private static readonly Dictionary<string, Func<IQueryable<Product>, ProductFilterParams, IQueryable<Product>>> _productFilterMethods = new()
        {
            { "brand", FilterByBrands },
            { "category", FilterByCategories },
            { "demographic", FilterByDemographics },
            { "price", FilterByPrice },
            { "color", FilterByColors },
            { "material", FilterByMaterials }
        };

        /// <summary>
        /// Returns the product with a given Id.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="productId"></param>
        /// <returns>The matching product.</returns>
        public static IQueryable<Product> WhereProductIdEquals(this IQueryable<Product> products, int productId)
        {
            return products.Where(p => p.Id == productId).AsQueryable();
        }

        /// <summary>
        /// Extension method that applies each of the filters
        ///  specified in <c>filterParams.FilterBy</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>
        /// The filtered products;
        /// returns all products if no filter given,
        /// and returns no products given any invalid filter.
        /// </returns>
        /// <exception cref="BadRequestException"></exception>
        public static IQueryable<Product> ApplyProductFilters(this IQueryable<Product> products, ProductFilterParams filterParams)
        {
            if (filterParams.FilterBy == null) return products;
            foreach (var filter in filterParams.FilterBy)
            {
                try
                {
                    products = _productFilterMethods[filter](products, filterParams);
                }
                catch (KeyNotFoundException)
                {
                    // This is the easiest way to get an empty result.
                    return products.Take(0);
                }
            }
            return products;
        }

        /// <summary>
        /// Filters a collection of products to only include
        /// those whose <c>.Brand</c> property matches one of the brands
        /// given in <c>filterParams.Brands</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>The filtered products.</returns>
        public static IQueryable<Product> FilterByBrands(IQueryable<Product> products, ProductFilterParams filterParams)
        {
            if (filterParams.Brands == null) return products;
            return products.Where(p => (p.Brand != null) && filterParams.Brands.Contains(p.Brand.ToLower()));
        }

        /// <summary>
        /// Filters a collection of products to only include
        /// those whose <c>.Category</c> property matches one of the categories
        /// given in <c>filterParams.Categories</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>The filtered products.</returns>
        public static IQueryable<Product> FilterByCategories(IQueryable<Product> products, ProductFilterParams filterParams)
        {
            if (filterParams.Categories == null) return products;
            return products.Where(p => p.Category != null && filterParams.Categories.Contains(p.Category.ToLower()));
        }

        /// <summary>
        /// Filters a collection of products to only include
        /// those whose <c>.Demographic</c> property matches one of the demographics
        /// given in <c>filterParams.Demographics</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>The filtered products.</returns>
        public static IQueryable<Product> FilterByDemographics(IQueryable<Product> products, ProductFilterParams filterParams)
        {
            if (filterParams.Demographics == null) return products;
            return products.Where(p => p.Demographic != null && filterParams.Demographics.Contains(p.Demographic.ToLower()));
        }

        /// <summary>
        /// Filters a collection of products to only include
        /// those whose <c>.Price</c> property is
        /// both greater than or equal to all of the values
        /// in <c>filterParams.MinPrices</c>,
        /// and less than or equal to all of the values
        /// in <c>filterParams.MaxPrices</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>The filtered products.</returns>
        public static IQueryable<Product> FilterByPrice(IQueryable<Product> products, ProductFilterParams filterParams)
        {
            var min = filterParams.MinPrices.Max();
            var max = filterParams.MaxPrices.Min();
            return products.Where(p => min <= p.Price && p.Price <= max);
        }

        /// <summary>
        /// Filters a collection of products to only include
        /// those whose <c>.Color</c> property matches one of the colors
        /// given in <c>filterParams.Colors</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>The filtered products.</returns>
        public static IQueryable<Product> FilterByColors(IQueryable<Product> products, ProductFilterParams filterParams)
        {
            if (filterParams.Colors == null) return products;
            return products.Where(p => ((p.PrimaryColorCode != null) && filterParams.Colors.Contains(p.PrimaryColorCode))
                                       || ((p.SecondaryColorCode != null) && filterParams.Colors.Contains(p.SecondaryColorCode)));
        }

        /// <summary>
        /// Filters a collection of products to only include
        /// those whose <c>.Material</c> property matches one of the materials
        /// given in <c>filterParams.Materials</c>.
        /// </summary>
        /// <param name="products"></param>
        /// <param name="filterParams"></param>
        /// <returns>The filtered products.</returns>
        public static IQueryable<Product> FilterByMaterials(IQueryable<Product> products, ProductFilterParams filterParams)
        {
            if (filterParams.Materials == null) return products;
            return products.Where(p => (p.Material != null) && filterParams.Materials.Contains(p.Material.ToLower()));
        }
    }
}
