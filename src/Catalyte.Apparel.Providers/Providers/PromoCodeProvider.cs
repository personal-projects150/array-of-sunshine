﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IPromoCodeProvider interface, providing service methods for promo codes.
    /// </summary>
    public class PromoCodeProvider : IPromoCodeProvider
    {
        private readonly ILogger<PromoCodeProvider> _logger;
        private readonly IPromoCodeRepository _promoCodeRepository;

        public PromoCodeProvider(IPromoCodeRepository promoCodeRepository, ILogger<PromoCodeProvider> logger)
        {
            _logger = logger;
            _promoCodeRepository = promoCodeRepository;
        }

        /// <summary>
        /// Persists a promo code to the database.
        /// </summary>
        /// <param name="model">PromoCodeDTO used to build the promo code.</param>
        /// <returns>The persisted promo code with IDs.</returns>
        public async Task<PromoCode> CreatePromoCodeAsync(PromoCode newPromoCode)
        {
      
            PromoCode savedPromoCode;

            if (newPromoCode.Type != "flat" && newPromoCode.Type != "percent")
            {
                //_logger.LogError("Valid promotional codes are either percent or flat.");
                throw new ConflictException("Valid promotional codes are either percent or flat.");
            }

            if (newPromoCode.Type == "percent")
            {
                int x = Int32.Parse(newPromoCode.Rate);
                if (x < 1 || x > 99)
                {
                 //_logger.LogError("Percentage off should be a whole number between 1 and 100");
                 throw new ConflictException("Percentage off should be a whole number between 1 and 100");
                }
            }

            if (newPromoCode.Type == "flat")
            {
                Regex rx = new Regex(@"\d+\.\d\d(?!\d)");
                if (!rx.IsMatch(newPromoCode.Rate))
                { 
                 //_logger.LogError("Flat dollar amount should be exactly two decimal places");
                throw new ConflictException("Flat dollar amount should be exactly two decimal places");

                }
            }
           
            try
            {
                savedPromoCode = await _promoCodeRepository.CreatePromoCodeAsync(newPromoCode);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }


            return savedPromoCode;
        }
    }

}