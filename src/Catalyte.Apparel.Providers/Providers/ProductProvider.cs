﻿using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.SeedData;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IProductProvider interface, providing service methods for products.
    /// </summary>
    public class ProductProvider : IProductProvider
    {
        private readonly ILogger<ProductProvider> _logger;
        private readonly IProductRepository _productRepository;



        public ProductProvider(IProductRepository productRepository, ILogger<ProductProvider> logger)
        {
            _logger = logger;
            _productRepository = productRepository;

        }

        /// <summary>
        /// Asynchronously retrieves the product with the provided id from the database.
        /// </summary>
        /// <param name="productId">The id of the product to retrieve.</param>
        /// <returns>The product.</returns>
        public async Task<Product> GetProductByIdAsync(int productId)
        {
            Product product;

            try
            {
                product = await _productRepository.GetProductByIdAsync(productId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (product == null || product == default)
            {
                _logger.LogInformation($"Product with id: {productId} could not be found.");
                throw new NotFoundException($"Product with id: {productId} could not be found.");
            }

            return product;
        }

        /// <summary>
        /// Asynchronously retrieves all products from the database.
        /// </summary>
        /// <returns>All products in the database.</returns>
#nullable enable
        public async Task<IEnumerable<Product>> GetProductsAsync(ProductFilterParams? filterParams = null)
        {
            if (filterParams == null) filterParams = new ProductFilterParams();
            IEnumerable<Product> products;

            try
            {
                products = await _productRepository.GetProductsAsync(filterParams);
            }
            catch (BadRequestException ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return products;
        }

        /// <summary>
        /// Asynchronously retrieves a set of filtered, paginated, active products from the database.
        /// </summary>
        /// <param name="params">parameters for pagination</param>
        /// <param name="filterParams">parameters for filtering</param>
        /// <returns>The requested products.</returns>
#nullable enable
        public async Task<IEnumerable<Product>> GetActiveFilteredPagedProductsAsync(PaginationParams @params, ProductFilterParams? filterParams = null)
        {
            if (filterParams == null) filterParams = new ProductFilterParams();
            IEnumerable<Product> products;

            try
            {
                products = await _productRepository.GetActiveFilteredPagedProductsAsync(@params, filterParams);
            }
            catch (BadRequestException ex)
            {
                _logger.LogError(ex.Message);
                throw;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return products;
        }

        /// <summary>
        /// Creates a new product and saves it in the database. Throws error if null, or if there is a problem connecting to database.
        /// </summary>
        /// <param name="newProduct"></param> new Product sent in from productController to add to database
        /// <returns>savedProduct from the database which will be sent to productController</returns>
        public async Task<Product> CreateProductAsync(Product newProduct)
        {


            ValidateProductInputFields(newProduct);

            if (newProduct == null)
            {
                _logger.LogError("Product must have value.");
                throw new BadRequestException("Product must have value.");
            }

            var myProductFactory = new ProductFactory();

            newProduct.Sku = myProductFactory.GetRandomSku();
            newProduct.GlobalProductCode = myProductFactory.GetRandomProductId();
            newProduct.DateCreated = DateTime.Now;
            newProduct.DateModified = DateTime.Now;

            Product savedProduct;

            try
            {
                savedProduct = await _productRepository.CreateProductAsync(newProduct);

                _logger.LogInformation("Product saved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return savedProduct;
        }

        /// <summary>
        /// If a create product field is invalid, 
        /// the error message is added to a list of Bad Request errors to be thrown simultaneously
        /// when this function is called (see CreateProductAsync)
        /// </summary>
        /// <param name="newProduct">Product DTO used to build the product</param>
        /// <exception cref="BadRequestException">string of bad request exceptions that have been added to the list</exception>
        public void ValidateProductInputFields(Product newProduct)
        {
            List<string> productExceptions = new();


            if (ValidateIfEmptyOrNull(newProduct.Name))
            {
                productExceptions.Add("Name is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.Brand))
            {
                productExceptions.Add("Brand is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.Category))
            {
                productExceptions.Add("Category is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.Type))
            {
                productExceptions.Add("Type is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.Material))
            {
                productExceptions.Add("Material is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.Description))
            {
                productExceptions.Add("Description is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.Demographic))
            {
                productExceptions.Add("Demographic is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.PrimaryColorCode))
            {
                productExceptions.Add("Primary Color Code is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.SecondaryColorCode))
            {
                productExceptions.Add("Secondary Color Code is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.StyleNumber))
            {
                productExceptions.Add("Style Number is required.");
            }
            if (ValidateIfEmptyOrNull(newProduct.ImageSrc))
            {
                productExceptions.Add("Image Source is required.");
            }
            if (productExceptions.Count > 0)
            {
                _logger.LogInformation(" ", productExceptions);
                throw new BadRequestException(String.Join(" ", productExceptions));
            }
        }
        /// <summary>
        /// Validates if string input field is null or empty
        /// </summary>
        /// <param name="modelField">string input field</param>
        /// <returns>boolean, true if input field is null or empty</returns>

        public bool ValidateIfEmptyOrNull(string modelField)
        {
            return string.IsNullOrWhiteSpace(modelField);
        }

        public async Task<Product> DeleteProductByIdAsync(int productId)
        {
            if (await IsDeleteable(productId))
            {
                var product = await _productRepository.DeleteProductByIdAsync(productId);
                return product;
            }
            throw new BadRequestException("Product has reviews and cannot be deleted");
        }
        public async Task<bool> IsDeleteable(int productId)
        {
            var product = await GetProductByIdAsync(productId);
            var reviews = product.Reviews;
            if (reviews.Count == 0)
            {
                return true;
            }
            return false;
        }
        public async Task<Product> ToggleProductActiveStateByIdAsync(int productId)
        {
            var product = await _productRepository.ToggleProductActiveStateByIdAsync(productId);
            return product;
        }


        public async Task<Product> UpdateProductByIdAsync(Product newProduct, int productId)
        {
            try
            {
                newProduct = await _productRepository.UpdateProductByIdAsync(newProduct, productId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return newProduct;
        }

    }

}
