﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IPurchaseProvider interface, providing service methods for purchases.
    /// </summary>
    public class PurchaseProvider : IPurchaseProvider
    {
        private readonly ILogger<PurchaseProvider> _logger;
        private readonly IPurchaseRepository _purchaseRepository;
        private readonly IProductRepository _productRepository;

        public PurchaseProvider(IPurchaseRepository purchaseRepository, ILogger<PurchaseProvider> logger, IProductRepository productRepository)
        {
            _logger = logger;
            _purchaseRepository = purchaseRepository;
            _productRepository = productRepository;

        }

        /// <summary>
        /// Retrieves all purchases from the database.
        /// </summary>
        /// <param name="email">BillingEmail associated with purchases.</param>
        /// <returns>All purchases with BillingEmail that matches email.</returns>
        public async Task<IEnumerable<Purchase>> GetAllPurchasesAsync(string email)
        {
            List<Purchase> purchases;

            try
            {
                purchases = await _purchaseRepository.GetAllPurchasesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return purchases.Where(p => p.BillingEmail == email);
        }

        /// <summary>
        /// Persists a purchase to the database.
        /// </summary>
        /// <param name="model">PurchaseDTO used to build the purchase.</param>
        /// <returns>The persisted purchase with IDs.</returns>
        public async Task<Purchase> CreatePurchasesAsync(Purchase newPurchase)
        {
            Purchase savedPurchase;

            await InactiveProductException(newPurchase);

            ValidatePurchaseInputFields(newPurchase);
            newPurchase.BillingZip = RemoveSpecialCharactersFromNumericalString(newPurchase.BillingZip);
            newPurchase.DeliveryZip = RemoveSpecialCharactersFromNumericalString(newPurchase.DeliveryZip);
            newPurchase.BillingPhone = RemoveSpecialCharactersFromNumericalString(newPurchase.BillingPhone);
            try
            {
                savedPurchase = await _purchaseRepository.CreatePurchaseAsync(newPurchase);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return savedPurchase;
        }


        ///<summary>
        ///Fixes bug where inactive products were able to be purchased
        /// </summary>
        /// <param name="newPurchase">Purchase being attempted</param>
        /// <returns>Message stating which products are inactive and unable to be purchased, if applicable</returns>
        public async Task<Purchase> InactiveProductException(Purchase newPurchase)
        {
            var cartItems = newPurchase.LineItems;
            var inactiveItems = new List<string>();
            var nullItems = new List<int>();

            foreach (var cartItem in cartItems)
            {
                Product cartProduct;
                try
                {
                    cartProduct = await _productRepository.GetProductByIdAsync(cartItem.ProductId);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex.Message);
                    throw new ServiceUnavailableException("There was a problem connecting to the database.");
                }

                if (cartProduct == null)
                {
                    nullItems.Add(cartItem.ProductId);
                }
                else if (cartProduct.Active == false)
                {
                    inactiveItems.Add(cartProduct.Name);
                }
            }

            if (nullItems.Count == 1)
            {
                var nullItemsString = string.Join(",", nullItems);
                var singleNullItemMessage = string.Format($"A product with ID# {nullItemsString} does not exist in our database.");
                _logger.LogInformation($"{singleNullItemMessage}");
                throw new UnprocessableEntityException($"{singleNullItemMessage}");
            }

            if (nullItems.Count > 1)
            {
                var multipleNullItems = string.Join(",", nullItems);
                var multipleNullItemMessage = string.Format($"Products with ID#s {multipleNullItems} do not exist in our database.");
                _logger.LogInformation($"{multipleNullItemMessage}");
                throw new UnprocessableEntityException($"{multipleNullItemMessage}");
            }

            if (inactiveItems.Count == 1)
            {
                var inactiveItemsString = string.Join(",", inactiveItems);
                var singleItemPayloadMessage = string.Format($"The following product is inactive and therefore cannot be purchased: {inactiveItemsString}.");
                _logger.LogInformation($"{singleItemPayloadMessage}");
                throw new UnprocessableEntityException($"{singleItemPayloadMessage}");

            }
            if (inactiveItems.Count > 1)
            {
                var inactiveItemsTogether = string.Join(",", inactiveItems);
                var payloadMessage = string.Format($"The following products are inactive and therefore cannot be purchased: {inactiveItemsTogether}.");
                _logger.LogInformation($"{payloadMessage}");
                throw new UnprocessableEntityException($"{payloadMessage}");
            }
            return newPurchase;
        }

        /// <summary>
        /// If a credit card field, delivery address field, or billing address field is invalid, 
        /// the error message is added to a list of Bad Request errors to be thrown simultaneously
        /// when this function is called (see CreatePurchaseAsync)
        /// </summary>
        /// <param name="newPurchase">Purchase DTO used to build the purchase</param>
        /// <exception cref="BadRequestException">string of bad request exceptions that have been added to the list</exception>
        public void ValidatePurchaseInputFields(Purchase newPurchase)
        {
            List<string> purchaseExceptions = new();


            if (ValidateIfEmptyOrNull(newPurchase.CardHolder))
            {
                purchaseExceptions.Add("Card Holder is required.");
            }
            else if (!ValidateIfAlphabeticHyphenAndApostropheOnly(newPurchase.CardHolder))
            {
                purchaseExceptions.Add("Card Holder name must contain only alphabetic letters, hyphens(-), or apostrophes(').");
            }
            if (ValidateIfEmptyOrNull(newPurchase.CardNumber))
            {
                purchaseExceptions.Add("Card Number is required.");
            }
            else if (!ValidateCreditCardSixteenNumericalDigits(newPurchase.CardNumber))
            {
                purchaseExceptions.Add("Card Number must be 16 numerical digits.");
            }
            else if (!ValidateCreditCardIsLuhnValid(newPurchase.CardNumber))
            {
                purchaseExceptions.Add("Card Number entered must be a valid credit card number.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.CVV))
            {
                purchaseExceptions.Add("CVV is required.");
            }
            else if (!ValidateCVVIsThreeNumericOnlyDigits(newPurchase.CVV))
            {
                purchaseExceptions.Add("CVV must contain 3 numerical digits.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.Expiration))
            {
                purchaseExceptions.Add("Expiration Date is required.");
            }
            else if (!ValidateExpirationDateFormat(newPurchase.Expiration))
            {
                purchaseExceptions.Add("Expiration date format is MM/YY.");
            }
            else if (!ValidateExpirationDateIsInTheFuture(newPurchase.Expiration))
            {
                purchaseExceptions.Add("The credit card has expired.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.BillingStreet))
            {
                purchaseExceptions.Add("Billing Street is required.");
            }
            else if (!ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(newPurchase.BillingStreet))
            {
                purchaseExceptions.Add("Billing Street can only contain letters, apostrophes('), hyphens(-), periods(.), and digits.");
            }
            if (!ValidateIfEmptyOrNull(newPurchase.BillingStreet2))
            {
                if (!ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(newPurchase.BillingStreet2))
                {
                    purchaseExceptions.Add("Billing Street 2 can only contain letters, apostrophes('), hyphens(-), periods(.), and digits.");
                }
            }
            if (ValidateIfEmptyOrNull(newPurchase.DeliveryStreet))
            {
                purchaseExceptions.Add("Delivery Street is required.");
            }
            else if (!ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(newPurchase.DeliveryStreet))
            {
                purchaseExceptions.Add("Delivery Street can only contain letters, apostrophes('), hyphens(-), periods(.), and digits.");
            }
            if (!ValidateIfEmptyOrNull(newPurchase.DeliveryStreet2))
            {
                if (!ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(newPurchase.DeliveryStreet2))
                {
                    purchaseExceptions.Add("Delivery Street 2 can only contain letters, apostrophes('), hyphens(-), periods(.), and digits.");
                }
            }
            if (ValidateIfEmptyOrNull(newPurchase.BillingCity))
            {
                purchaseExceptions.Add("Billing City is Required.");
            }
            else if (!ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(newPurchase.BillingCity))
            {
                purchaseExceptions.Add("Billing City can only contain letters, apostrophes('), hyphens(-), periods(.).");
            }
            if (ValidateIfEmptyOrNull(newPurchase.DeliveryCity))
            {
                purchaseExceptions.Add("Delivery City is required.");
            }
            else if (!ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(newPurchase.DeliveryCity))
            {
                purchaseExceptions.Add("Delivery City can only contain letters, apostrophes('), hyphens(-), periods(.).");
            }
            if (ValidateIfEmptyOrNull(newPurchase.DeliveryFirstName))
            {
                purchaseExceptions.Add("First Name is required.");
            }
            else if (!ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(newPurchase.DeliveryFirstName))
            {
                purchaseExceptions.Add("First Name can only contain letters, apostrophes('), hyphens(-), periods(.).");
            }
            if (ValidateIfEmptyOrNull(newPurchase.DeliveryLastName))
            {
                purchaseExceptions.Add("Last Name is required.");
            }
            else if (!ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(newPurchase.DeliveryLastName))
            {
                purchaseExceptions.Add("Last Name can only contain letters, apostrophes('), hyphens(-), periods(.).");
            }
            if (ValidateIfEmptyOrNull(newPurchase.BillingZip))
            {
                purchaseExceptions.Add("Billing Zip Code is required.");
            }
            else if (!ValidateZipCodeIsOnlyFiveOrNineDigits(newPurchase.BillingZip))
            {
                purchaseExceptions.Add("Billing Zip Code must be 5 or 9 digits.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.DeliveryZip))
            {
                purchaseExceptions.Add("Delivery Zip Code is required.");
            }
            else if (!ValidateZipCodeIsOnlyFiveOrNineDigits(newPurchase.DeliveryZip))
            {
                purchaseExceptions.Add("Delivery Zip Code must be 5 or 9 digits.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.BillingEmail))
            {
                purchaseExceptions.Add("Billing Email is required.");
            }
            else if (!ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(newPurchase.BillingEmail))
            {
                purchaseExceptions.Add("Billing Email must be formatted user@email.com.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.BillingPhone))
            {
                purchaseExceptions.Add("Phone number is required.");
            }
            else if (!ValidatePhoneNumberIsTenDigits(newPurchase.BillingPhone))
            {
                purchaseExceptions.Add("Phone number must be 10 digits e.g. (123) 456-7890.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.DeliveryState))
            {
                purchaseExceptions.Add("Delivery State is required.");
            }
            if (ValidateIfEmptyOrNull(newPurchase.BillingState))
            {
                purchaseExceptions.Add("Billing State is required.");
            }
            if (purchaseExceptions.Count > 0)
            {
                _logger.LogInformation(" ", purchaseExceptions);
                throw new BadRequestException(String.Join(" ", purchaseExceptions));
            }
        }

        /// <summary>
        /// Validates if string input field is null or empty
        /// </summary>
        /// <param name="modelField">string input field</param>
        /// <returns>boolean, true if input field is null or empty</returns>

        public bool ValidateIfEmptyOrNull(string modelField)
        {
            return string.IsNullOrWhiteSpace(modelField);
        }

        /// <summary>
        /// validates if a string only contains alphabetic characters, hyphens, and apostrophes. Allows spaces. (FIRST AND LAST NAMES)
        /// </summary>
        /// <param name="modelField">string name input field</param>
        /// <returns>boolean, true if input matches regex</returns>
        public bool ValidateIfAlphabeticHyphenAndApostropheOnly(string modelField)
        {
            var nameCheck = new Regex(@"^[\p{L} \'\-]+$");
            return nameCheck.IsMatch(modelField);
        }

        /// <summary>
        /// validates if a string only contains alphabetic characters, hyphens, periods, and apostrophes. Allows spaces. (CITY)
        /// </summary>
        /// <param name="modelField">string name input field</param>
        /// <returns>boolean, true if input matches regex</returns>
        public bool ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(string modelField)
        {
            var nameCheck = new Regex(@"^[\p{L} \.'\-]+$");
            return nameCheck.IsMatch(modelField);
        }

        /// <summary>
        /// Validates if a string only contains alphabetic charactes, hyphens, apostrophes, periods, and digits only. Allows spaces. (STREETS/ADDRESS)
        /// </summary>
        /// <param name="modelField">string input field</param>
        /// <returns>boolean, true if input matches regex</returns>
        public bool ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(string modelField)
        {
            var nameCheck = new Regex(@"^[\p{L} \dg\.'\-]+$");
            return nameCheck.IsMatch(modelField);
        }

        /// <summary>
        /// Validates if a credit card string contains only 16 numerical digits
        /// </summary>
        /// <param name="modelField">string credit card input</param>
        /// <returns>boolean, true if credit card string matches regex</returns>
        public bool ValidateCreditCardSixteenNumericalDigits(string modelField)
        {
            var cardNumberCheck = new Regex(@"^[0-9]{16}$");
            return cardNumberCheck.IsMatch(modelField);
        }

        /// <summary>
        /// Validates if credit card number passes Luhn algorithm (also called Mod 10)
        /// </summary>
        /// <param name="cardNum">a credit card number string</param>
        /// <returns>boolean, true or false if credit card nnumber is valid or not</returns>
        public bool ValidateCreditCardIsLuhnValid(string cardNum)
        {
            int nDigits = cardNum.Length;

            int nSum = 0;
            bool isSecond = false;
            for (int i = nDigits - 1; i >= 0; i--)
            {
                int d = cardNum[i] - '0';

                if (isSecond == true)
                    d *= 2;

                nSum += d / 10;
                nSum += d % 10;

                isSecond = !isSecond;
            }
            return (nSum % 10 == 0);
        }

        /// <summary>
        /// Validates if CVV input field contains only 3 numerical digits
        /// </summary>
        /// <param name="cvvNum">string CVV number input field</param>
        /// <returns>boolean, true if string input matches regex</returns>
        public bool ValidateCVVIsThreeNumericOnlyDigits(string cvvNum)
        {
            var cvvCheck = new Regex(@"^[0-9]{3}$");
            return cvvCheck.IsMatch(cvvNum);
        }

        /// <summary>
        /// Validates if expiration date is properly formatted as MM/YY
        /// with MM held to constant 2 digits (02 for February)
        /// </summary>
        /// <param name="expirationDate">string credit card expiration date input field</param>
        /// <returns>boolean, FALSE if both MM and YY match regex, TRUE if either or both do not match regex</returns>
        public bool ValidateExpirationDateFormat(string expirationDate)
        {
            CultureInfo enUS = new CultureInfo("en-US");
            return DateTime.TryParseExact(expirationDate, "MM/yy", enUS, DateTimeStyles.None, out _);
        }

        /// <summary>
        /// Validates if credit card expiration date is in the future
        /// </summary>
        /// <param name="expirationDate">string credit card expiration date input field</param>
        /// <returns>boolean, true if expiration date is in the future</returns>
        public bool ValidateExpirationDateIsInTheFuture(string expirationDate)//MM/YY
        {
            var dateParts = expirationDate.Split('/');//split date on / per valid formatting
            var year = int.Parse(dateParts[1]);//year is second in the split
            var month = int.Parse(dateParts[0]);//month is first in the split
            var lastDateOfExpiryMonth = DateTime.DaysInMonth(year, month); //get actual expiration date
            var cardExpiration = new DateTime((year + 2000), month, lastDateOfExpiryMonth, 23, 59, 59);//23 days, 59 minutes, 59 seconds to my understanding

            return cardExpiration > DateTime.Now;
        }

        /// <summary>
        /// Validates if user email address is formatted correctly (user@email.com) with alphanumeric username and alphatic domain (EMAIL)
        /// </summary>
        /// <param name="emailAddress">string email input field</param>
        /// <returns>boolean, true if the email is formatted correctly</returns>
        public bool ValidateEmailIsAlphaNumericUserNameAndAlphabeticDomain(string emailAddress)
        {
            var emailCheck = new Regex(@"^\w+@([a-z]+\.)+[a-z]+$");
            return emailCheck.IsMatch(emailAddress);
        }

        /// <summary>
        /// Validates if user billing/delivery zipcode is only four or five numerical digits (ZIPCODE)
        /// </summary>
        /// <param name="zipCode">string zipcode input field</param>
        /// <returns>boolean, true if the zipcode is valid</returns>
        public bool ValidateZipCodeIsOnlyFiveOrNineDigits(string zipCode)
        {
            var zipCodeCheck = new Regex(@"^[0-9]{5}(?:-[0-9]{4})?$");
            return zipCodeCheck.IsMatch(zipCode);
        }

        /// <summary>
        ///Validates if user phone number is 10 numberical digits (PHONE NUMBER)
        /// </summary>
        /// <param name="phoneNumber">string phone unmber input field</param>
        /// <returns>boolean, true if the phone number is valid</returns>

        public bool ValidatePhoneNumberIsTenDigits(string phoneNumber)
        {
            var phoneNumberCheck = new Regex(@"^\(?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}$");
            return phoneNumberCheck.IsMatch(phoneNumber);
        }

        /// <summary>
        /// Removes special characters so only numerical digits remain (for removing UI only dashes/parantheses/spaces/etc. from number strings before
        /// persisting to the database)
        /// </summary>
        /// <param name="numericalInputField">string numerical input field (such as phone number or zip code)</param>
        /// <returns>string with special characters removed</returns>
        public string RemoveSpecialCharactersFromNumericalString(string numericalInputField)
        {
            return Regex.Replace(numericalInputField, "[^0-9]+", "");
        }
    }
}