﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;


namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IProductReviewProvider interface, providing service methods for product reviews.
    /// </summary>
    public class ProductReviewProvider : IProductReviewProvider
    {
        private readonly ILogger<ProductReviewProvider> _logger;
        private readonly IProductReviewRepository _productReviewRepository;
        private readonly IProductProvider _productProvider;
        private readonly IUserRepository _userRepository;

        public ProductReviewProvider(IProductReviewRepository productReviewRepository, ILogger<ProductReviewProvider> logger, IProductRepository @object, IProductProvider productProvider, IUserRepository userRepository)
        {
            _logger = logger;
            _productReviewRepository = productReviewRepository;
            _productProvider = productProvider;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Asynchronously retrieves the product review with the provided id from the database.
        /// </summary>
        /// <param name="productReviewId">The id of the product review to retrieve.</param>
        /// <returns>The review.</returns>
        public async Task<ProductReview> GetProductReviewByIdAsync(int productReviewId)
        {
            ProductReview productReview;

            try
            {
                productReview = await _productReviewRepository.GetProductReviewByIdAsync(productReviewId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (productReview == null || productReview == default)
            {
                _logger.LogInformation($"Product Review with id: {productReviewId} could not be found.");
                throw new NotFoundException($"Product Review with id: {productReviewId} could not be found.");
            }
            return productReview;
        }

        /// <summary>
        /// Asynchronously retrieves all product reviews from the database.
        /// </summary>
        /// <returns>All reviews in the database.</returns>
        public async Task<IEnumerable<ProductReview>> GetAllProductReviewsAsync()
        {
            IEnumerable<ProductReview> productsReviews;

            try
            {
                productsReviews = await _productReviewRepository.GetAllProductReviewsAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return productsReviews;
        }

        /// <summary>
        /// Asynchronously retrieves the product reviews with the provided product id from the database.
        /// </summary>
        /// <param name="productId">The id of the product for which to retrieve reviews.</param>
        /// <returns>The reviews.</returns>
        public async Task<IEnumerable<ProductReview>> GetProductReviewsByProductIdAsync(int productId)
        {
            IEnumerable<ProductReview> productReviews;
            try
            {
                var product = await _productProvider.GetProductByIdAsync(productId);
            }
            catch (NotFoundException ex)
            {
                _logger.LogInformation($"NotFoundException in ProductReviewProvider.GetProductReviewsByProductIdAsync: {ex.Message}");
                throw;
            }

            try
            {
                productReviews = await _productReviewRepository.GetProductReviewsByProductIdAsync(productId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return productReviews;
        }
        /// <summary>
        /// Creates new Product Review by first validating all fields, then adding Date posted time.now, and saving review to Database
        /// </summary>
        /// <param name="newProductReview">New review posted by user</param>
        /// <returns>Saves valid review in the database</returns>
        public async Task<ProductReview> CreateProductReviewAsync(ProductReview newProductReview)
        {

            ValidateProductReview(newProductReview);

            try
            {
                var product = await _productProvider.GetProductByIdAsync((int)newProductReview.ProductId);
            }
            catch (NotFoundException ex)
            {
                _logger.LogInformation($"NotFoundException in ProductReviewProvider.GetProductReviewsByProductIdAsync: {ex.Message}");
                throw;
            }
            var user = await _userRepository.GetUserByIdAsync((int)newProductReview.UserId);

            if (user == null || user == default)
            {
                _logger.LogInformation("User not found.");
                throw new NotFoundException("User not found.");
            }


            newProductReview.DatePosted = DateTime.Now;
            newProductReview.DateModified = newProductReview.DatePosted;
            newProductReview.DateCreated = newProductReview.DatePosted;

            ProductReview savedProductReview;

            try
            {
                savedProductReview = await _productReviewRepository.CreateProductReviewAsync(newProductReview);

                _logger.LogInformation("Review saved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return savedProductReview;
        }

        /// <summary>
        /// Validates a review, throwing an error for invalid fields
        /// </summary>
        /// <param name="productReview"></param>
        /// <exception cref="BadRequestException"></exception>
        public void ValidateProductReview(ProductReview productReview)
        {
            if (productReview.Rating == 0)
            {
                _logger.LogError("Rating must have a numerical value between 1-5.");
                throw new BadRequestException("Rating must have a numerical value between 1-5.");
            }
            else if (productReview.Rating < 1 || productReview.Rating > 5)
            {
                _logger.LogError("Rating must be between 1 and 5.");
                throw new BadRequestException("Rating must be between 1 and 5.");
            }
            else if (productReview.Rating % 1 != 0)
            {
                _logger.LogError("Rating must be a whole number between 1 and 5.");
                throw new BadRequestException("Rating must be a whole number between 1 and 5.");
            }
            else if (productReview.Title == null || productReview.Title.Trim().Length == 0)
            {
                _logger.LogError("Title must have value.");
                throw new BadRequestException("Title must have value.");
            }
            else if (productReview.Title.Length > 100)
            {
                _logger.LogError("Title must be less than 100 characters.");
                throw new BadRequestException("Title must be less than 100 characters.");
            }
            else if (productReview.Comment == null || productReview.Comment.Trim().Length == 0)
            {
                _logger.LogError("Comment must have value.");
                throw new BadRequestException("Comment must have value.");
            }
            else if (productReview.Comment.Length > 2000)
            {
                _logger.LogError("Comment must be less than 2000 characters.");
                throw new BadRequestException("Comment must be less than 2000 characters.");
            }
            else if (productReview.UserId == null || productReview.UserId == 0)
            {
                _logger.LogError("UserId must have value.");
                throw new BadRequestException("UserId must have value.");
            }
            else if (productReview.ProductId == null || productReview.ProductId == 0)
            {
                _logger.LogError("ProductId must have value.");
                throw new BadRequestException("ProductId must have value.");
            };
        }

        /// <summary>
        /// Updates the review with the provided ID.
        /// </summary>
        /// <param name="id">ID of review to update</param>
        /// <param name="updatedReview">New data to store in review</param>
        /// <returns>The updated review</returns>
        /// <exception cref="ServiceUnavailableException"></exception>
        /// <exception cref="NotFoundException"></exception>
        public async Task<ProductReview> UpdateProductReviewAsync(int id, ProductReview updatedReview)
        {
            updatedReview.Id = id;
            ValidateProductReview(updatedReview);
            ProductReview existingReview;

            try
            {
                existingReview = await _productReviewRepository.GetProductReviewByIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (existingReview == default)
            {
                _logger.LogInformation($"Review with id: {id} does not exist.");
                throw new NotFoundException($"Review with id:{id} not found.");
            }

            // TIMESTAMP THE UPDATE
            updatedReview.DatePosted = existingReview.DatePosted;
            updatedReview.DateCreated = existingReview.DateCreated;
            updatedReview.DateModified = DateTime.UtcNow;

            try
            {
                await _productReviewRepository.UpdateProductReviewAsync(updatedReview);
                _logger.LogInformation("Review updated.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return updatedReview;
        }

        public async Task<ProductReview> DeleteProductReviewByIdAsync(int productReviewId)
        {
            var productReview = await _productReviewRepository.DeleteProductReviewByIdAsync(productReviewId);
            if (productReview == null || productReview == default)
            {
                _logger.LogInformation($"Product Review with id: {productReviewId} could not be found.");
                throw new NotFoundException($"Product Review with id: {productReviewId} could not be found.");
            }
            return productReview;    
        }

    }
}


