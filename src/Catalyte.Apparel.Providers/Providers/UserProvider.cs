﻿using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities;
using Catalyte.Apparel.Providers.Auth;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IUserProvider interface, providing service methods for users.
    /// </summary>
    public class UserProvider : IUserProvider
    {
        private readonly ILogger<UserProvider> _logger;
        private readonly IUserRepository _userRepository;
        private readonly GoogleAuthService googleAuthService = new();

        public UserProvider(
            IUserRepository userRepository,
            ILogger<UserProvider> logger
        )
        {
            _userRepository = userRepository;
            _logger = logger;
        }

        public async Task<User> GetUserByEmailAsync(string email)
        {
            User user;

            try
            {
                user = await _userRepository.GetUserByEmailAsync(email);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (user == default)
            {
                _logger.LogError($"Could not find user with email: {email}");
                throw new NotFoundException($"Could not find user with email: {email}");
            }

            return user;
        }

        /// <summary>
        /// Persists updated user information given they have provided the correct credentials.
        /// </summary>
        /// <param name="bearerToken">String value in the "Authorization" property of the header.</param>
        /// <param name="id">Id of the user to update.</param>
        /// <param name="updatedUser">Updated user information to persist.</param>
        /// <returns>The updated user.</returns>
        public async Task<User> UpdateUserAsync(string bearerToken, int id, User updatedUser)
        {
            // AUTHENTICATES USER - SAME EMAIL, SAME PERSON
            // Authenticating the user ensures that the user is using Google to sign in
            /* string token = googleAuthService.GetTokenFromHeader(bearerToken);
            bool isAuthenticated = await googleAuthService.AuthenticateUserAsync(token, updatedUser);
            if (!isAuthenticated)
            {
                _logger.LogError("Email in the request body does not match email from the JWT Token");
                throw new BadRequestException("Email in the request body does not match email from JWT Token");
            }
            */
            // VALIDATE USER TO UPDATE EXISTS
            User existingUser;

            try
            {
                existingUser = await _userRepository.GetUserByIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (existingUser == default)
            {
                _logger.LogInformation($"User with id: {id} does not exist.");
                throw new NotFoundException($"User with id:{id} not found.");
            }

            // TEMPORARY LOGIC TO PREVENT USER FROM UPDATING THEIR ROLE
            updatedUser.Role = existingUser.Role;

            // GIVE THE USER ID IF NOT SPECIFIED IN BODY TO AVOID DUPLICATE USERS
            if (updatedUser.Id == default)
                updatedUser.Id = id;

            ValidateUserUpdateFields(updatedUser);
            // TIMESTAMP THE UPDATE
            updatedUser.DateModified = DateTime.UtcNow;

                try
            {
                await _userRepository.UpdateUserAsync(updatedUser);
                _logger.LogInformation("User updated.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return updatedUser;
        }

        /// <summary>
        /// If a user trying to update their profile information enters invalid information, 
        /// the error message is added to a list of Bad Request errors to be thrown simultaneously
        /// when this function is called (see UpdateUserAsync)
        /// </summary>
        /// <param name="updatedUser"></param>

        public void ValidateUserUpdateFields(User updatedUser)
        {
            List<string> updateExceptions = new();

            if (!ValidateIfAlphabeticHyphenAndApostropheOnly(updatedUser.FirstName))
            {
                updateExceptions.Add("First name must contain only alphabetic letters, hyphens(-), or apostrophes(').");
            }
            if (!ValidateIfAlphabeticHyphenAndApostropheOnly(updatedUser.LastName))
            {
                updateExceptions.Add("Last name must contain only alphabetic letters, hyphens(-), or apostrophes(').");
            }
            if (!ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(updatedUser.Street))
            {
                updateExceptions.Add("Street can only contain letters, apostrophes('), hyphens(-), periods(.), and digits.");
            }
            if (!ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(updatedUser.City))
            {
                updateExceptions.Add("City can only contain letters, apostrophes('), hyphens(-), periods(.).");
            }
            if (!ValidateZipCodeIsOnlyFiveOrNineDigits(updatedUser.Zip))
            {
                updateExceptions.Add("Billing Zip Code must be 5 or 9 digits.");
            }
            if (updateExceptions.Count > 0)
            {
                _logger.LogInformation(" ", updateExceptions);
                throw new BadRequestException(String.Join(" ", updateExceptions));
            }
        }

        /// <summary>
        /// validates if a string only contains alphabetic characters, hyphens, and apostrophes. Allows spaces. (FIRST AND LAST NAMES)
        /// </summary>
        /// <param name="modelField">string name input field</param>
        /// <returns>boolean, true if input matches regex</returns>
        public bool ValidateIfAlphabeticHyphenAndApostropheOnly(string modelField)
        {
            var nameCheck = new Regex(@"^[\p{L} \'\-]+$");
            return nameCheck.IsMatch(modelField);
        }

        /// <summary>
        /// validates if a string only contains alphabetic characters, hyphens, periods, and apostrophes. Allows spaces. (CITY)
        /// </summary>
        /// <param name="modelField">string name input field</param>
        /// <returns>boolean, true if input matches regex</returns>
        public bool ValidateIfAlphabeticHyphenApostropheAndPeriodOnly(string modelField)
        {
            var nameCheck = new Regex(@"^[\p{L} \.'\-]+$");
            return nameCheck.IsMatch(modelField);
        }

        /// <summary>
        /// Validates if a string only contains alphabetic charactes, hyphens, apostrophes, periods, and digits only. Allows spaces. (STREETS/ADDRESS)
        /// </summary>
        /// <param name="modelField">string input field</param>
        /// <returns>boolean, true if input matches regex</returns>
        public bool ValidateIfAlphabeticHyphenApostrophePeriodAndDigitsOnly(string modelField)
        {
            var nameCheck = new Regex(@"^[\p{L} \dg\.'\-]+$");
            return nameCheck.IsMatch(modelField);
        }

        /// <summary>
        /// Validates if user billing/delivery zipcode is only four or five numerical digits (ZIPCODE)
        /// </summary>
        /// <param name="zipCode">string zipcode input field</param>
        /// <returns>boolean, true if the zipcode is valid</returns>
        public bool ValidateZipCodeIsOnlyFiveOrNineDigits(string zipCode)
        {
            var zipCodeCheck = new Regex(@"^[0-9]{5}(?:-[0-9]{4})?$");
            return zipCodeCheck.IsMatch(zipCode);
        }

        /// <summary>
        /// Persists a user to the database given the provided email is not already in the database or null.
        /// </summary>
        /// <param name="user">The user to persist.</param>
        /// <returns>The user.</returns>
        public async Task<User> CreateUserAsync(User newUser)
        {
            if (newUser.Email == null)
            {
                _logger.LogError("User must have an email field.");
                throw new BadRequestException("User must have an email field");
            }

            // CHECK TO MAKE SURE THE USE EMAIL IS NOT TAKEN
            User existingUser;

            try
            {
                existingUser = await _userRepository.GetUserByEmailAsync(newUser.Email);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            if (existingUser != default)
            {
                _logger.LogError("Email is taken.");
                throw new ConflictException("Email is taken");
            }

            // SET DEFAULT ROLE TO CUSTOMER AND TIMESTAMP
            newUser.Role = Constants.CUSTOMER;
            newUser.DateCreated = DateTime.UtcNow;
            newUser.DateModified = DateTime.UtcNow;

            User savedUser;

            try
            {
                savedUser = await _userRepository.CreateUserAsync(newUser);
                _logger.LogInformation("User saved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }

            return savedUser;
        }

    }
}
