﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.Data.Interfaces;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Providers.Interfaces;
using Catalyte.Apparel.Utilities.HttpResponseExceptions;
using Microsoft.Extensions.Logging;


namespace Catalyte.Apparel.Providers.Providers
{
    /// <summary>
    /// This class provides the implementation of the IWishlistProvider interface, providing service methods for wishlist items.
    /// </summary>
    public class WishlistProvider : IWishlistProvider
    {
        private readonly ILogger<WishlistProvider> _logger;
        private readonly IWishlistRepository _wishlistRepository;
        private readonly IProductProvider _productProvider;
        private readonly IUserRepository _userRepository;

        public WishlistProvider(IWishlistRepository wishlistRepository, ILogger<WishlistProvider> logger, IProductProvider productProvider, IUserRepository userRepository)
        {
            _logger = logger;
            _wishlistRepository = wishlistRepository;
            _productProvider = productProvider;
            _userRepository = userRepository;
        }

        /// <summary>
        /// Creates new wishlist item linked to user and saves to database
        /// </summary>
        /// <param name="addedProduct">Product added to wishlist by user</param>
        /// <returns>Saves product in the database</returns>
        /// 
        public async Task<WishlistItem> AddToWishlistAsync(WishlistItem addedProduct)
        {
            WishlistItem savedProduct;

            if (addedProduct == null)
            {
                _logger.LogError("Wishlist item must have a value.");
                throw new BadRequestException("Wishlist item must have a value.");
            }

            if (await ValidateSingleWishlistItem(addedProduct))
            {
                _logger.LogError("Wishlist items cannot be duplicate.");
                throw new BadRequestException("Wishlist items cannot be duplicate.");
            }

            try
            {
                savedProduct = await _wishlistRepository.AddToWishlistAsync(addedProduct);

                _logger.LogInformation("Wishlist item saved.");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return savedProduct;
        }

        /// <summary>
        /// Asynchronously retrieves wishlist items with the provided user id from the database.
        /// </summary>
        /// <param name="userId">The id of the user for which to retrieve wishlist items.</param>
        /// <returns>The wishlist items.</returns>
        public async Task<List<WishlistItem>> GetWishlistByUserIdAsync(int userId)
        {
            List<WishlistItem> wishlistItems;
            try
            {
                var user = await _userRepository.GetUserByIdAsync(userId);
            }
            catch (NotFoundException ex)
            {
                _logger.LogInformation($"NotFoundException in WishlistProvider.GetWishlistByIdAsync: {ex.Message}");
                throw;
            }

            try
            {
                wishlistItems = await _wishlistRepository.GetWishlistByUserIdAsync(userId);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                throw new ServiceUnavailableException("There was a problem connecting to the database.");
            }
            return wishlistItems;
        }
        public async Task<WishlistItem> DeleteWishlistItemByIdAsync(int wishlistItemId)
        {
            var wishlistItem = await _wishlistRepository.DeleteWishlistItemByIdAsync(wishlistItemId);
            if (wishlistItem == null || wishlistItem == default)
            {
                _logger.LogInformation($"Wishlist item with id: {wishlistItemId} could not be found.");
                throw new NotFoundException($"Wishlist item with id: {wishlistItemId} could not be found.");
            }
            return wishlistItem;
        }

        public async Task<bool> ValidateSingleWishlistItem(WishlistItem addedProduct)
        {
            var wishedItemList = await GetWishlistByUserIdAsync(addedProduct.UserId);
            var wishedItemProdIdList = new List<int>();
            foreach (var item in wishedItemList)
            {
                wishedItemProdIdList.Add(item.ProductId);
            }

            if (wishedItemProdIdList.Contains(addedProduct.ProductId))
            {
                return true;
            }
            return false;
        }

    }
}