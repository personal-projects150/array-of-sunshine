﻿using Catalyte.Apparel.Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for product review related service methods.
    /// </summary>
    public interface IProductReviewProvider
    {
        Task<ProductReview> GetProductReviewByIdAsync(int productReviewId);
        Task<ProductReview> CreateProductReviewAsync(ProductReview model);
        Task<IEnumerable<ProductReview>> GetAllProductReviewsAsync();
        Task<IEnumerable<ProductReview>> GetProductReviewsByProductIdAsync(int productId);
        Task<ProductReview> DeleteProductReviewByIdAsync(int productReviewId);
        Task<ProductReview> UpdateProductReviewAsync(int id, ProductReview review);
    }
}