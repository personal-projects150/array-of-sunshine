﻿using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.WishlistItems;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for product review related service methods.
    /// </summary>
    public interface IWishlistProvider
    {
        Task<List<WishlistItem>> GetWishlistByUserIdAsync(int userId);
        Task<WishlistItem> AddToWishlistAsync(WishlistItem wishlistItem);
        Task<WishlistItem> DeleteWishlistItemByIdAsync(int wishlistItemId);
    }
}