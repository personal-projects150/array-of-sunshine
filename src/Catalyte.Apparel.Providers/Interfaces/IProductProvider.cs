﻿using Catalyte.Apparel.Data.Filters;
using Catalyte.Apparel.Data.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalyte.Apparel.Providers.Interfaces
{
    /// <summary>
    /// This interface provides an abstraction layer for product related service methods.
    /// </summary>
    public interface IProductProvider
    {
        Task<Product> GetProductByIdAsync(int productId);

        Task<Product> CreateProductAsync(Product product);

#nullable enable
        Task<IEnumerable<Product>> GetProductsAsync(ProductFilterParams? filterParams = null);

        Task<IEnumerable<Product>> GetActiveFilteredPagedProductsAsync(PaginationParams @params, ProductFilterParams? filterParams = null);

        Task<Product> DeleteProductByIdAsync(int productId);

        Task<Product> UpdateProductByIdAsync(Product newProduct, int productId);
        Task<Product> ToggleProductActiveStateByIdAsync(int productId);

    }
}
