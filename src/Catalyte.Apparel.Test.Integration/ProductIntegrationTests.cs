using Catalyte.Apparel.DTOs.Products;
using Catalyte.Apparel.Test.Integration.Utilities;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    [Collection("Sequential")]
    public class ProductIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _client;//Postman in code

        public ProductIntegrationTests(CustomWebApplicationFactory factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions//initialize the above
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public async Task GetProducts_Returns200()
        {
            var response = await _client.GetAsync("/products");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<ProductDTO>>();
            Assert.Equal(1000, content.Count);
        }

        [Fact]
        public async Task GetProductById_GivenByExistingId_Returns200()
        {
            var response = await _client.GetAsync("/products/1");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<ProductDTO>();
            Assert.Equal(1, content.Id);
        }

        [Fact]
        public async Task GetUniqueCategories_Returns200()
        {
            var response = await _client.GetAsync("/products/categories");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<string>>();

            Assert.Equal(9, content.Count);
        }

        [Fact]
        public async Task GetUniquetTypes_Returns200()
        {
            var response = await _client.GetAsync("/products/types");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<string>>();
            Assert.Single(content);
        }



        [Fact]
        public async Task FilterProductsByCategory_ReturnsExpectedProducts()
        {
            var response = await _client.GetAsync("/products?filterBy=category&category=golf");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<ProductDTO>>();
            Assert.All(content, p => Assert.Equal("Golf", p.Category));
        }

        [Fact]
        public async Task FilterProductsByKey_WithNoMatch_ReturnsNoProducts()
        {
            var response = await _client.GetAsync("/products?filterBy=demographic&demographic=SNAILS");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<ProductDTO>>();
            Assert.Empty(content);
        }

        [Fact]
        public async Task FilterProductsByMisspelledKey_ReturnsNoProducts()
        {
            var response = await _client.GetAsync("/products?filterBy=crategory&category=golf");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<ProductDTO>>();
            Assert.Empty(content);
        }

        [Fact]
        public async Task FilterProductsByTwoKeys_ReturnsExpectedProducts()
        {
            string url = "/products?filterBy=demographic&filterBy=category&demographic=Men&category=Boxing";
            var response = await _client.GetAsync(url);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<List<ProductDTO>>();
            Assert.All(content, p => Assert.True(p.Category == "Boxing" && p.Demographic == "Men"));
        }

        /// <summary>
        /// Tests that when given a valid product, a created and saved product is returned.
        /// </summary>
        /// <returns>Saved product from database</returns>
        [Fact]
        public async Task CreateProductAsync__WithProduct_ReturnsCreatedProduct()
        {
            var testProduct = new ProductDTO
            {
                Name = "Test Product",
                Description = "Next Gen Soccer Shoe made for motivated Women.",
                Demographic = "Women",
                Category = "Soccer",
                Type = "Shoe",
                ReleaseDate = System.DateTime.Today,
                PrimaryColorCode = "#39add1",
                SecondaryColorCode = "#f9845b",
                StyleNumber = "SCUIDFG",
                Active = true,
                Brand = "Nike",
                ImageSrc = "https://source.unsplash.com/b_S4jZlgtLo",
                Material = "Nylon",
                Price = 250,
                Quantity = 50
            };
            var newProductDTO = JsonContent.Create(testProduct);
            var response = await _client.PostAsync("/products", newProductDTO);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }
        [Fact]
        public async Task DeleteProductAsync_WithNonexistantProduct_Returns404()
        {
            var response = await _client.DeleteAsync("/products/999789");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task DeleteProductAsync_WithDeleteableProduct_Returns200()
        {
            var testProduct = new ProductDTO
            {
                Name = "Test Product",
                Description = "Next Gen Soccer Shoe made for motivated Women.",
                Demographic = "Women",
                Category = "Soccer",
                Type = "Shoe",
                ReleaseDate = System.DateTime.Today,
                PrimaryColorCode = "#39add1",
                SecondaryColorCode = "#f9845b",
                StyleNumber = "SCUIDFG",
                Active = true,
                Brand = "Nike",
                ImageSrc = "https://source.unsplash.com/b_S4jZlgtLo",
                Material = "Nylon",
                Price = 250,
                Quantity = 50,
                Id = 24601
            };
            var newProductDTO = JsonContent.Create(testProduct);
            await _client.PostAsync("/products", newProductDTO);
            // Make sure product was created
            var createdProduct = await (await _client.GetAsync("/products/24601")).Content.ReadAsAsync<ProductDTO>();
            Assert.Equal("Test Product", createdProduct.Name);
            // Delete product
            var response = await _client.DeleteAsync("/products/24601");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
            // Make sure product was deleted
            var productDeleted = await _client.GetAsync("/products/24601");
            Assert.Equal(HttpStatusCode.NotFound, productDeleted.StatusCode);
        }
        [Fact]
        public async Task DeleteProductAsync_WithReviewedProduct_Returns400()
        {
            // TODO: Test not written because there was no way to add reviews to a product
        }
    }
}
