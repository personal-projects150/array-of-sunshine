﻿using Catalyte.Apparel.DTOs;
using Catalyte.Apparel.Test.Integration.Utilities;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    [Collection("Sequential")]
    public class PromoCodeIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _client;

        public PromoCodeIntegrationTests(CustomWebApplicationFactory factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public async Task CreatePromoCode_Returns201()
        {
            var PromoCodesDTO = new PromoCodesDTO
            {
                Title = "SUMMER2015",
                Description = "Our summer discount for the Q3 2015 campaign",
                Type = "flat",
                Rate = "500.00"
            };

            var promoCodesDTOJson = JsonContent.Create(PromoCodesDTO);

            var response = await _client.PostAsync("/promos", promoCodesDTOJson);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);

        }

        [Fact]
        public async Task CreateEmptyPromoCode_Returns415()
        {
            var response = await _client.PostAsync("/promos", null);
            Assert.Equal(HttpStatusCode.UnsupportedMediaType, response.StatusCode);

        }

        [Fact]
        public async Task CreateWrongPromoCode_ReturnsConflictException()
        {
            var PromoCodesDTO = new PromoCodesDTO
            {
                Title = "SUMMER2015",
                Description = "Our summer discount for the Q3 2015 campaign",
                Type = "flat",
                Rate = "500"
            };

            var promoCodesDTOJson = JsonContent.Create(PromoCodesDTO);

            var response = await _client.PostAsync("/promos", promoCodesDTOJson);
            Assert.Equal(HttpStatusCode.Conflict, response.StatusCode);
        }
    }
}
