﻿using Catalyte.Apparel.DTOs.ProductReviews;
using Catalyte.Apparel.Test.Integration.Utilities;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Xunit;

namespace Catalyte.Apparel.Test.Integration
{
    [Collection("Sequential")]
    public class ProductReviewIntegrationTests : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _client;

        public ProductReviewIntegrationTests(CustomWebApplicationFactory factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        /// <summary>
        /// Tests that valid review post returns a saved review.
        /// </summary>
        [Fact]
        public async Task CreateProductReviewAsync_WithReview_ReturnsCreatedReview()
        {
            var testReviewDTO = new ProductReviewDTO
            {
                Rating = 4,
                Title = "Instructions unclear",
                DateCreated = System.DateTime.Now,
                DateModified = System.DateTime.Now,
                DatePosted = System.DateTime.Now,
                Comment = "r sit amet, consectetur adipiscing elit. Integer eget facilisis magna, a convallis enim. Phasellus fermentum, justo et laoreet iaculis, orci augue malesuada quam, in porta enim est non ex. Aliquam imperdiet in dolor a cursus. Proin lobortis sagittis luctus.",
                ProductId = 340,
                UserId = 2
            };

            var newProductReviewDTO = JsonContent.Create(testReviewDTO);
            var response = await _client.PostAsync("/reviews", newProductReviewDTO);
            Assert.Equal(HttpStatusCode.Created, response.StatusCode);
        }

        /// <summary>
        /// Tests that invalid review post returns a bad request.
        /// </summary>
        [Fact]
        public async Task CreateProductReviewAsync_WithInvalidReview_ReturnsBadRequest()
        {
            var testReviewDTO = new ProductReviewDTO
            {
                Rating = 0,
                Title = "Instructions unclear",
                DateCreated = System.DateTime.Now,
                DateModified = System.DateTime.Now,
                DatePosted = System.DateTime.Now,
                Comment = "r sit amet, consectetur adipiscing elit. Integer eget facilisis magna, a convallis enim. Phasellus fermentum, justo et laoreet iaculis, orci augue malesuada quam, in porta enim est non ex. Aliquam imperdiet in dolor a cursus. Proin lobortis sagittis luctus.",
                ProductId = 340,
                UserId = 2
            };

            var newProductReviewDTO = JsonContent.Create(testReviewDTO);
            var response = await _client.PostAsync("/reviews", newProductReviewDTO);
            Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
        }
        [Fact]
        public async Task GetProductReviews_Returns200()
        {
            var response = await _client.GetAsync("/reviews");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetProductReviewById_GivenByExistingId_Returns200()
        {
            var response = await _client.GetAsync("/reviews/1");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var content = await response.Content.ReadAsAsync<ProductReviewDTO>();
            Assert.Equal(1, content.Id);
            Assert.IsType<ProductReviewDTO>(content);
        }

        [Fact]
        public async Task GetProductReviewById_WithInvalidId_Returns404()
        {
            var response = await _client.GetAsync("/reviews/99999");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetProductReviews_ByProductId_Returns200()
        {
            var response = await _client.GetAsync("/reviews?productId=1");
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }

        [Fact]
        public async Task GetProductReviews_ByProductId_WithInvalidProductId_Returns404()
        {
            var response = await _client.GetAsync("/reviews?productId=9999");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
        [Fact]
        public async Task DeleteProductReview_ByIdAsync_Returns204()
        {
            var response = await _client.DeleteAsync("/reviews/1");
            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
        }
        [Fact]
        public async Task DeleteProductReview_ByIdAsync_WithInvalidId_Returns404()
        {
            var response = await _client.DeleteAsync("/reviews/9999");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
