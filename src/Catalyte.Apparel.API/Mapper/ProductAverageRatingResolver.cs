﻿using System.Linq;
using AutoMapper;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.Products;

namespace Catalyte.Apparel.API.Mapper
{
    public class ProductAverageRatingResolver : IValueResolver<Product, ProductDTO, decimal>
    {
        public decimal Resolve(Product product, ProductDTO productDTO, decimal destMember, ResolutionContext context)
        {
            return (product.Reviews != null && product.Reviews.Count > 0)
                ? (decimal)product.Reviews.Select(r => r.Rating).Sum() / product.Reviews.Count
                : 0m;
        }
    }
}
