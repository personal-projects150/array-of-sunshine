﻿using AutoMapper;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.Purchases;
using Catalyte.Apparel.DTOs.WishlistItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Catalyte.Apparel.API.DTOMappings
{
    public static class MapperExtensions
    {

        public static IEnumerable<PurchaseDTO> MapPurchasesToPurchaseDtos(this IMapper mapper, IEnumerable<Purchase> purchases)
        {
            return purchases
                .Select(x => mapper.MapPurchaseToPurchaseDto(x))
                .ToList();
        }

        /// <summary>
        /// Helper method to build model for a purchase DTO.
        /// </summary>
        /// <param name="purchase">The purchase to be persisted.</param>
        /// <returns>A purchase DTO.</returns>
        public static PurchaseDTO MapPurchaseToPurchaseDto(this IMapper mapper, Purchase purchase)
        {
            return new PurchaseDTO()
            {
                Id = purchase.Id,
                OrderDate = purchase.OrderDate,
                LineItems = mapper.Map<List<LineItemDTO>>(purchase.LineItems),
                DeliveryAddress = mapper.Map<DeliveryAddressDTO>(purchase),
                BillingAddress = mapper.Map<BillingAddressDTO>(purchase),
                CreditCard = mapper.Map<CreditCardDTO>(purchase),
                PurchaseTotal = purchase.PurchaseTotal
            };
        }

        public static Purchase MapCreatePurchaseDtoToPurchase(this IMapper mapper, CreatePurchaseDTO purchaseDTO)
        {
            var purchase = new Purchase
            {
                OrderDate = DateTime.UtcNow,
            };
            purchase = mapper.Map(purchaseDTO.DeliveryAddress, purchase);
            purchase = mapper.Map(purchaseDTO.BillingAddress, purchase);
            purchase = mapper.Map(purchaseDTO.CreditCard, purchase);
            purchase.LineItems = mapper.Map(purchaseDTO.LineItems, purchase.LineItems);
            purchase.PurchaseTotal = purchaseDTO.PurchaseTotal;
            return purchase;
        }

        public static IEnumerable<WishlistItemDTO> MapWishlistItemsToWishlistItemDtos(this IMapper mapper, IEnumerable<WishlistItem> wishlistItems)
        {
            return wishlistItems
                .Select(x => mapper.MapWishlistItemToWishlistItemDto(x))
                .ToList();
        }

        public static WishlistItemDTO MapWishlistItemToWishlistItemDto(this IMapper mapper, WishlistItem wishlistItem)
        {
            return new WishlistItemDTO()
            {
                Id = wishlistItem.Id,
                UserId = wishlistItem.UserId,
                ProductId = wishlistItem.ProductId,
                ProductName = wishlistItem.ProductName
            };
        }

        public static WishlistItem MapWishlistItemDtoToWishlistItem(this IMapper mapper, WishlistItemDTO wishlistItemDTO)
        {
            var wishItem = new WishlistItem();
            wishItem = mapper.Map(wishlistItemDTO, wishItem);
            return wishItem;
        }
    }
}
