﻿using System.Linq;
using AutoMapper;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.Products;

namespace Catalyte.Apparel.API.Mapper
{
    public class ProductReviewCountResolver : IValueResolver<Product, ProductDTO, int>
    {
        public int Resolve(Product product, ProductDTO productDTO, int destMember, ResolutionContext context)
        {
            if (product.Reviews == null)
            {
                return 0;
            }
            return product.Reviews.Count();
        }
    }
}
