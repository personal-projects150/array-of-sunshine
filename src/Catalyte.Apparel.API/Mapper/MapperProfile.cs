﻿using AutoMapper;
using Catalyte.Apparel.API.Mapper;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs;
using Catalyte.Apparel.DTOs.ProductReviews;
using Catalyte.Apparel.DTOs.Products;
using Catalyte.Apparel.DTOs.Purchases;
using Catalyte.Apparel.DTOs.WishlistItems;

namespace Catalyte.Apparel.API
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Product, ProductDTO>()
                .ForMember(dest => dest.AverageRating, opt => opt.MapFrom<ProductAverageRatingResolver>())
                .ForMember(dest => dest.ReviewCount, opt => opt.MapFrom<ProductReviewCountResolver>())
                .ReverseMap();
            CreateMap<ProductReview, ProductReviewDTO>().ReverseMap();
            CreateMap<CreatePurchaseDTO, Purchase>();
            CreateMap<ProductReview, ProductReviewDTO>().ReverseMap();
            CreateMap<Purchase, PurchaseDTO>();
            CreateMap<Purchase, DeliveryAddressDTO>().ReverseMap();
            CreateMap<Purchase, CreditCardDTO>().ReverseMap();
            CreateMap<Purchase, BillingAddressDTO>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.BillingEmail))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.BillingPhone))
                .ReverseMap();
            
            CreateMap<LineItem, LineItemDTO>().ReverseMap();
            
            CreateMap<User, UserDTO>().ReverseMap();

            CreateMap<PromoCode, PromoCodesDTO>().ReverseMap();
            CreateMap<WishlistItem, WishlistItemDTO>().ReverseMap();
        }
    }
}
