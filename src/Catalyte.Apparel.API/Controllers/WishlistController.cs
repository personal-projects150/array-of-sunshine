using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Catalyte.Apparel.API.DTOMappings;
using Catalyte.Apparel.DTOs.WishlistItems;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The WishlistController exposes endpoints for wishlist related actions.
    /// </summary>
    [ApiController]
    [Route("/wishlist")]
    public class WishlistController : ControllerBase
    {
        private readonly ILogger<WishlistController> _logger;
        private readonly IWishlistProvider _wishlistProvider;
        private readonly IMapper _mapper;

        public WishlistController(
            ILogger<WishlistController> logger,
            IWishlistProvider wishlistProvider,
            IMapper mapper
        )
        {
            _logger = logger;
            _mapper = mapper;
            _wishlistProvider = wishlistProvider;
        }

        /// <summary>
        /// Sends product information and user ID to wishlist endpoint
        /// </summary>
        /// <param name="productToAdd"></param>
        /// <returns></returns>

        [HttpPost]
        public async Task<ActionResult<List<WishlistItemDTO>>> AddToWishlistAsync([FromBody] WishlistItemDTO productToAdd)
        {
            _logger.LogInformation("Request received for AddToWishlist");

            var newWish = _mapper.MapWishlistItemDtoToWishlistItem(productToAdd);
            var wishlistItem = await _wishlistProvider.AddToWishlistAsync(newWish);
            var wishedItemDTO = _mapper.MapWishlistItemToWishlistItemDto(wishlistItem);

            return Created("/wishlist", wishedItemDTO);
        }

        [HttpGet("{userId}")]
        public async Task<ActionResult<List<WishlistItemDTO>>> GetWishlistByUserIdAsync(int userId)
        {
            _logger.LogInformation("Request received for GetWishlistByIdAsync");

            var wishlistItems = await _wishlistProvider.GetWishlistByUserIdAsync(userId);
            var wishlistItemDTOs = _mapper.MapWishlistItemsToWishlistItemDtos(wishlistItems);

            return Ok(wishlistItemDTOs);
        }

        /// <summary>
        /// Finds a wishlist item by ID and removes it from the repository
        /// </summary>
        /// <param name="wishlistItemId">ID number associated with a review</param>
        /// <returns>The deleted review, or an error</returns>
        [HttpDelete("{wishlistItemId}")]
        public async Task<ActionResult<WishlistItemDTO>> DeleteWishlistItemByIdAsync(int wishlistItemId)
        {
            _logger.LogInformation("Request received for DeleteWishlistItemByIdAsync");
            try
            {
                var review = await _wishlistProvider.DeleteWishlistItemByIdAsync(wishlistItemId);
                return NoContent();
            }
            catch (System.ArgumentNullException)
            {
                return NotFound($"No item with ID: {wishlistItemId} exists in the database");
            }

        }
    }
}
