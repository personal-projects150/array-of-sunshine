﻿using AutoMapper;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The PromoCodeController exposes endpoints for promo code related actions.
    /// </summary>
    [ApiController]
    [Route("/promos")]
    public class PromoCodesController : ControllerBase
    {
        private readonly ILogger<PromoCodesController> _logger;
        private readonly IPromoCodeProvider _promoCodeProvider;
        private readonly IMapper _mapper;

        public PromoCodesController(
            ILogger<PromoCodesController> logger,
            IPromoCodeProvider promoCodeProvider,
            IMapper mapper
        )
        {
            _logger = logger;
            _promoCodeProvider = promoCodeProvider;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<PromoCodesDTO>> CreatePromoCodeAsync([FromBody] PromoCode promoCodeToCreate)
        {
            _logger.LogInformation("Request received for CreatePromoCodeAsync");

            var promoCode = await _promoCodeProvider.CreatePromoCodeAsync(promoCodeToCreate);
            var promoCodesDTO = _mapper.Map<PromoCodesDTO>(promoCode);

            return Created("/promos", promoCodesDTO);
        }
    }
}
