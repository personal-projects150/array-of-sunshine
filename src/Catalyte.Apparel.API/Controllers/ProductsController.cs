﻿
using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.DTOs.Products;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.Data.Filters;
using System.Text.Json;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The ProductsController exposes endpoints for product related actions.
    /// </summary>
    [ApiController]
    [Route("/products")]
    public class ProductsController : ControllerBase
    {
        private readonly ILogger<ProductsController> _logger;
        private readonly IProductProvider _productProvider;
        private readonly IMapper _mapper;

        public ProductsController(
            ILogger<ProductsController> logger,
            IProductProvider productProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _productProvider = productProvider;
        }

        [HttpGet("categories")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetUniqueProductCategoriesAsync()
        {
            _logger.LogInformation("Request received for GetProductsAsync");

            var products = await _productProvider.GetProductsAsync();
            var categories = new List<string>();

            foreach (Product product in products)
            {
                if (!categories.Contains(product.Category))
                {
                    categories.Add(product.Category);
                }
            }

            return Ok(categories);
        }

        [HttpGet("types")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetUniqueProductTypesAsync()
        {
            _logger.LogInformation("Request received for GetProductsAsync");

            var products = await _productProvider.GetProductsAsync();
            var types = new List<string>();

            foreach (Product product in products)
            {
                if (!types.Contains(product.Type))
                {
                    types.Add(product.Type);
                }
            }

            return Ok(types);
        }

        [HttpGet("Count")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProductCount([FromQuery] ProductFilterParams filterParams)
        {
            _logger.LogInformation("Request received for GetProductsAsync");

            var products = await _productProvider.GetProductsAsync(filterParams);
            var productDTOs = _mapper.Map<IEnumerable<ProductDTO>>(products).Where(product => product.Active)
                .OrderBy(p => p.Id);

            var productCount = productDTOs.Count();

            return Ok(productCount);
        }

        [HttpGet("active")]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetActivePagedProductsAsync([FromQuery] ProductFilterParams filterParams, [FromQuery] PaginationParams @params)
        {
            _logger.LogInformation("Request received for GetProductsAsync");

            var products = await _productProvider.GetActiveFilteredPagedProductsAsync(@params, filterParams);
            var productDTOs = _mapper.Map<IEnumerable<ProductDTO>>(products);

            var paginationMeta = new PaginationMeta(productDTOs.Count(), @params.Page, @params.ItemsPerPage);
            Response.Headers.Add("X-Pagination", JsonSerializer.Serialize(paginationMeta));

            return Ok(productDTOs);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductDTO>>> GetProductsAsync([FromQuery] ProductFilterParams filterParams)
        {
            _logger.LogInformation("Request received for GetProductsAsync");

            var products = await _productProvider.GetProductsAsync(filterParams);
            var productDTOs = _mapper.Map<IEnumerable<ProductDTO>>(products);

            return Ok(productDTOs);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductDTO>> GetProductByIdAsync(int id)
        {
            _logger.LogInformation($"Request received for GetProductByIdAsync for id: {id}");

            var product = await _productProvider.GetProductByIdAsync(id);
            var productDTO = _mapper.Map<ProductDTO>(product);

            return Ok(productDTO);
        }

        /// <summary>
        /// Gets new Product from frontend and maps through the CreatePurchaseAsync method as a product and then maps it back to a productDTO to return to the frontend.
        /// </summary>
        [HttpPost("/products")]
        public async Task<ActionResult<ProductDTO>> CreateProductAsync([FromBody] ProductDTO newProductForm)
        {
            var newProduct = _mapper.Map<Product>(newProductForm);
            _logger.LogInformation("Request received for CreateProductAsync");
            var product = await _productProvider.CreateProductAsync(newProduct);
            var productDTO = _mapper.Map<ProductDTO>(product);

            return Created("/products", productDTO);
        }

        /// <summary>
        /// Finds a product by ID and removes it from the repository if it has no reviews
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The deleted product, or an error</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductDTO>> DeleteProductByIdAsync(int id)
        {
            _logger.LogInformation("Request received for DeleteProductByIdAsync");
            try
            {
                var product = await _productProvider.DeleteProductByIdAsync(id);
                return Ok(product);
            }
            catch (System.ArgumentNullException)
            {
                return NotFound($"No product with ID: {id} exists in the database");
            }

        }

        [HttpPut("{productId}")]
        public async Task<ActionResult<ProductDTO>> UpdateProductByIdAsync(Product newProduct, int productId)
        {
            _logger.LogInformation("Request received for UpdateProductByIdAsync");
            try
            {
                var updatedProduct = await _productProvider.UpdateProductByIdAsync(newProduct, productId);
                var productDTO = _mapper.Map<ProductDTO>(updatedProduct);
                return Ok(updatedProduct);
            }
            catch (System.ArgumentNullException)
            {
                return NotFound($"No product with ID: {productId} exists in the database");
            }
        }

        /// <summary>
        /// Finds a product by ID and toggles active status.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Returns inactive status, or an error</returns>
        [HttpPut("activeProducts/{id}")]
        public async Task<ActionResult<ProductDTO>> ToggleProductActiveStateByIdAsync(int id)
        {
            _logger.LogInformation("Request received for ToggleProductActiveStateByIdAsync");
            try
            {
                var product = await _productProvider.ToggleProductActiveStateByIdAsync(id);
                var productDTO = _mapper.Map<ProductDTO>(product);
                return productDTO;
            }
            catch (System.ArgumentNullException)
            {
                return NotFound($"No product with ID: {id} exists in the database");
            }

        }
    }
}
