﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using Catalyte.Apparel.Providers.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Catalyte.Apparel.Data.Model;
using Catalyte.Apparel.DTOs.ProductReviews;

namespace Catalyte.Apparel.API.Controllers
{
    /// <summary>
    /// The ProductReviewsController exposes endpoints for review related actions.
    /// </summary>
    [ApiController]
    [Route("/reviews")]
    public class ProductReviewsController : ControllerBase
    {
        private readonly ILogger<ProductReviewsController> _logger;
        private readonly IProductReviewProvider _productReviewProvider;
        private readonly IMapper _mapper;

        public ProductReviewsController(
            ILogger<ProductReviewsController> logger,
            IProductReviewProvider productReviewProvider,
            IMapper mapper)
        {
            _logger = logger;
            _mapper = mapper;
            _productReviewProvider = productReviewProvider;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductReviewDTO>>> GetProductReviewsAsync([FromQuery] int? productId)
        {
            _logger.LogInformation("Request received for GetProductReviewsAsync");

            IEnumerable<ProductReview> reviews;

            if (productId == null) reviews = await _productReviewProvider.GetAllProductReviewsAsync();
            else reviews = await _productReviewProvider.GetProductReviewsByProductIdAsync((int)productId);

            var productReviewDTOs = _mapper.Map<IEnumerable<ProductReviewDTO>>(reviews);

            return Ok(productReviewDTOs);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProductReviewDTO>> GetProductReviewByIdAsync(int id)
        {
            _logger.LogInformation($"Request received for GetProductReviewByIdAsync for id: {id}");

            var product = await _productReviewProvider.GetProductReviewByIdAsync(id);
            var productDTO = _mapper.Map<ProductReviewDTO>(product);

            return Ok(productDTO);
        }

        /// <summary>
        /// Controls a HTTP Post method for adding a new product review to the database
        /// </summary>
        /// <param name="newReviewToCreate">new review posted by a user</param>
        /// <returns>saved productreviewDTO to the endpoint</returns>
        [HttpPost]
        public async Task<ActionResult<ProductReviewDTO>> CreateProductReviewAsync([FromBody] ProductReviewDTO newReviewToCreate)
        {
            var newReview = _mapper.Map<ProductReview>(newReviewToCreate);
            _logger.LogInformation("Request received for CreateProductReviewAsync");

            var productReview = await _productReviewProvider.CreateProductReviewAsync(newReview);
            var productReviewDTO = _mapper.Map<ProductReviewDTO>(productReview);

            return Created("/reviews", productReviewDTO);
        }

        /// <summary>
        /// Finds a review by ID and updates the content
        /// </summary>
        /// <param name="id">ID of review to update</param>
        /// <param name="newReview">New content</param>
        /// <returns>The updated review</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<ProductReviewDTO>> UpdateProductReviewAsync(int id, [FromBody] ProductReviewDTO newReview)
        {
            _logger.LogInformation("Request received for UpdateProductReviewAsync");

            var review = _mapper.Map<ProductReview>(newReview);
            var updatedReview = await _productReviewProvider.UpdateProductReviewAsync(id, review);
            var reviewDTO = _mapper.Map<ProductReviewDTO>(updatedReview);

            return Ok(reviewDTO);
        }
        /// <summary>
        /// Finds a review by ID and removes it from the repository
        /// </summary>
        /// <param name="productReviewId">ID number associated with a review</param>
        /// <returns>The deleted review, or an error</returns>
        [HttpDelete("{productReviewId}")]
        public async Task<ActionResult<ProductReviewDTO>> DeleteProductReviewByIdAsync(int productReviewId)
        {
            _logger.LogInformation("Request received for DeleteProductReviewByIdAsync");
            try
            {
                var review = await _productReviewProvider.DeleteProductReviewByIdAsync(productReviewId);
                return NoContent();
            }
            catch (System.ArgumentNullException)
            {
                return NotFound($"No review with ID: {productReviewId} exists in the database");
            }

        }

    }
}
